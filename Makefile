# compiler and linker
CC := g++

# target binary program
TARGET := prog

# directories
SRCDIR := src
INCDIR := include
BUILDDIR := build
TARGETDIR := bin
SRCEXT := cc
INCEXT := hh
OBJEXT := o

#INC += -I `root-config --incdir`
INC += -I `geant4-config --prefix`/include/Geant4
INC += -I include/
LIBS += -l CLHEP
LIBS += -l G4global -l G4tracking -l G4run -l G4materials -l G4geometry -l G4particles -l G4processes -l G4event -l G4RayTracer -l G4digits_hits
LIBS += -l G4interfaces -l G4intercoms -l G4graphics_reps -l G4vis_management -l G4OpenGL
LIBS += -l G4physicslists

src := $(wildcard src/*.${SRCEXT})

interim := $(subst .${SRCEXT},.${OBJEXT},${src})
CPPFLAGS += -Wall -Wextra -pedantic -ggdb
CXXFLAGS += `root-config --cflags`
LDFLAGS += `root-config --glibs`

all: prog

${SRCDIR}/%.o: ${SRCDIR}/%.${SRCEXT} ${INCDIR}/%.${INCEXT}
#${OBJDIR}/%.o: ${SRCDIR}/%.${SRCEXT} ${INCDIR}/%.${INCEXT}
	${CC} ${CPPFLAGS} ${CXXFLAGS} ${INC} $< -c -o $@

prog: main.cc ${interim}
#	g++ ${CPPFLAGS} ${INC} ${LIBS} $^ -o $@
	${CC} ${CPPFLAGS} ${CXXFLAGS} ${INC} ${LDFLAGS} ${LIBS} $^ -o $@

clean:
	rm prog ${SRCDIR}/*.${OBJEXT}

.PHONY: all clean

