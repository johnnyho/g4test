#include "HadronRunAction.hh"
#include "HadronAnalysisManager.hh"
#include "G4Run.hh"
#include "G4NistManager.hh"
#include "G4ParticleTable.hh"
#include "G4HadronicProcessStore.hh"

HadronRunAction::HadronRunAction()
{}

HadronRunAction::~HadronRunAction()
{}

void HadronRunAction::BeginOfRunAction(const G4Run* aRun)
{
    const G4int runID = aRun->GetRunID();
    // std::cout << "HadronRunAction::BeginOfRunAction: Run #" << runID << " start." << std::endl;
    HadronAnalysisManager * analysisManager = HadronAnalysisManager::getInstance();
    analysisManager->Book(runID);
    analysisManager->SetRun(runID);
}

void HadronRunAction::EndOfRunAction(const G4Run* aRun)
{ 
    const G4int runID = aRun->GetRunID();
    // std::cout<<"HadronRunAction::EndOfRunAction: end of run"<<std::endl;
    HadronAnalysisManager * analysisManager = HadronAnalysisManager::getInstance();

    if (runID == 1)
    {
        const G4String materialName = analysisManager->GetTargetMaterialName();
        const G4String elementName = materialName.substr(materialName.length()-2);
        const G4String particleName = analysisManager->GetParticleName();

        const G4Element* element = G4NistManager::Instance()->FindOrBuildElement(elementName);
        const G4Material* material = G4NistManager::Instance()->FindOrBuildMaterial(analysisManager->GetTargetMaterialName());
        const G4ParticleDefinition* particle = G4ParticleTable::GetParticleTable()->FindParticle(analysisManager->GetParticleName());

        G4HadronicProcessStore* store = G4HadronicProcessStore::Instance();

        // std::cout << materialName << std::endl;
        // std::cout << elementName << std::endl;
        // std::cout << particleName << std::endl;

        const double dE = 1.0 * CLHEP::MeV;
        const unsigned int n = 10000+1;

        for (unsigned int i = 0; i < n; ++i)
        {
            const double energy = i * dE;
            const double xsElastic = store->GetElasticCrossSectionPerAtom(particle, energy, element, material) / CLHEP::barn;
            const double xsInelastic = store->GetInelasticCrossSectionPerAtom(particle, energy, element, material) / CLHEP::barn;
            const double xsTotal = xsElastic + xsInelastic;

            // std::cout << energy << " " << xsElastic << " " << xsInelastic << " " << xsTotal << std::endl;

            analysisManager->FillCrossSection(energy, xsElastic, xsInelastic, xsTotal);
        }
    }

    if (runID > 0) analysisManager->Save();
}
