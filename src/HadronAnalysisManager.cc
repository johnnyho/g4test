#include "HadronAnalysisManager.hh"
#include "HadronAnalysisMessenger.hh"

HadronAnalysisManager * HadronAnalysisManager::instance = 0;

//-----------------------------------------------------------------------------
HadronAnalysisManager::HadronAnalysisManager()
{
#ifdef G4ANALYSIS_USE
#endif
    outputDirectory = "./output";
    saveSteps = true;
    analysisMessenger = new HadronAnalysisMessenger(this);
}

//-----------------------------------------------------------------------------
HadronAnalysisManager::~HadronAnalysisManager()
{
#ifdef G4ANALYSIS_USE
#endif
    delete analysisMessenger;
}

//-----------------------------------------------------------------------------
HadronAnalysisManager * HadronAnalysisManager::getInstance()
{
    if (instance == 0) instance = new HadronAnalysisManager;
    return instance;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::Book(const G4int runID)
{
    // std::cout << "HAHAHA" << std::endl;
    // std::cout << "Run ID: " << runID << std::endl;

    // if (runID > 0) fileName = "./output/tfile_" + std::to_string(runID) + ".root";
    if (runID > 0) fileName = outputDirectory + "/tfile_" + std::to_string(runID) + ".root";
    else           fileName = "/tmp/tfile.root";
    tfile = new TFile(fileName.c_str(), "recreate", "hadron analysis");
    ttree = new TTree("HadronAnalysis", "hadron analysis");

    ttree->Branch("run", &run, "run/I");
    ttree->Branch("event", &event, "event/I");
    ttree->Branch("initialKineticEnergy", &initialKineticEnergy, "initialKineticEnergy/D");
    ttree->Branch("targetRadius", &targetRadius, "targetRadius/D");
    ttree->Branch("targetLength", &targetLength, "targetLength/D");
    ttree->Branch("targetMaxStep", &targetMaxStep, "targetMaxStep/D");
    ttree->Branch("elastic", &elastic, "elastic/I");
    ttree->Branch("inelastic", &inelastic, "inelastic/I");
    ttree->Branch("kineticEnergy", &kineticEnergy);
    ttree->Branch("energyDeposit", &energyDeposit);
    ttree->Branch("stepSize", &stepSize);

    ttreeXS = new TTree("CrossSection", "cross section");
    ttreeXS->Branch("energy", &energyXS, "energyXS/D");
    ttreeXS->Branch("elastic", &elasticXS, "elasticXS/D");
    ttreeXS->Branch("inelastic", &inelasticXS, "inelasticXS/D");
    ttreeXS->Branch("total", &totalXS, "totalXS/D");
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::Save()
{
    ttree->Write();
    if (run == 1) ttreeXS->Write();
    tfile->Close();
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::EventReset()
{
    event = -1;
    elastic = 0;
    inelastic = 0;
    kineticEnergy.clear();
    energyDeposit.clear();
    stepSize.clear();
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::EventFill()
{
    ttree->Fill();
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetOutputDirectory(const std::string value)
{
    outputDirectory = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetSaveSteps(const bool value)
{
    saveSteps = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetRun(const int value)
{
    run = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetEvent(const int value)
{
    event = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetElastic(const int value)
{
    elastic = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetInelastic(const int value)
{
    inelastic = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetInitialKineticEnergy(const double value)
{
    initialKineticEnergy = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetTargetRadius(const double value)
{
    targetRadius = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetTargetLength(const double value)
{
    targetLength = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetTargetMaxStep(const double value)
{
    targetMaxStep = value;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::AddKineticEnergy(const double value)
{
    kineticEnergy.push_back(value);
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::AddEnergyDeposit(const double value)
{
    energyDeposit.push_back(value);
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::AddStepSize(const double value)
{
    stepSize.push_back(value);
}

//-----------------------------------------------------------------------------
G4String HadronAnalysisManager::GetOutputDirectory()
{
    return outputDirectory;
}

//-----------------------------------------------------------------------------
G4bool HadronAnalysisManager::GetSaveSteps()
{
    return saveSteps;
}

//-----------------------------------------------------------------------------
bool HadronAnalysisManager::GetElastic()
{
    return elastic;
}

//-----------------------------------------------------------------------------
bool HadronAnalysisManager::GetInelastic()
{
    return inelastic;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetTargetMaterial(G4Material* material)
{
    targetMaterial = material;
    targetMaterialName = targetMaterial->GetName();
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::SetParticle(G4ParticleDefinition* particleDefinition)
{
    particle = particleDefinition;
    particleName = particle->GetParticleName();
}

//-----------------------------------------------------------------------------
G4String HadronAnalysisManager::GetTargetMaterialName()
{
    return targetMaterialName;
}

//-----------------------------------------------------------------------------
G4String HadronAnalysisManager::GetParticleName()
{
    return particleName;
}

//-----------------------------------------------------------------------------
void HadronAnalysisManager::FillCrossSection(const double energy, const double eXS, const double iXS, const double tXS)
{
    energyXS = energy;
    elasticXS = eXS;
    inelasticXS = iXS;
    totalXS = tXS;

    ttreeXS->Fill();
}

