
#include "HadronEventAction.hh"

#include "HadronAnalysisManager.hh"

#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4ios.hh"

HadronEventAction::HadronEventAction()
{}

HadronEventAction::~HadronEventAction()
{}

void HadronEventAction::BeginOfEventAction(const G4Event*)
{
    // HadronAnalysisManager * analysisManager = HadronAnalysisManager::getInstance();
}

void HadronEventAction::EndOfEventAction(const G4Event* event)
{
    G4int eventID = event->GetEventID();
    // if (eventID < 100 || eventID % 100 == 0) std::cout << "Event ID: " << eventID << std::endl;

    HadronAnalysisManager * analysisManager = HadronAnalysisManager::getInstance();
    analysisManager->SetEvent(eventID);
    analysisManager->EventFill();
    analysisManager->EventReset();
}

