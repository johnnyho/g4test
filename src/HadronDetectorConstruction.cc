
#include "HadronDetectorConstruction.hh"
#include "HadronDetectorMessenger.hh"
#include "HadronSensitiveDetector.hh"
#include "HadronAnalysisManager.hh"

#include "G4RunManager.hh"

#include "G4Material.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PVParameterised.hh"
#include "G4SDManager.hh"
#include "G4GeometryTolerance.hh"
#include "G4GeometryManager.hh"

#include "G4UserLimits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4ios.hh"

#include "G4NistManager.hh"

HadronDetectorConstruction::HadronDetectorConstruction() :
    solidWorld(0),  logicWorld(0),  physicalWorld(0),
    solidTarget(0), logicTarget(0), physicalTarget(0), 
    stepLimit(0), 
    maxStep(0.5*CLHEP::cm),
    targetRadius(0.3*CLHEP::cm),
    targetLength(4.0*0.5*CLHEP::cm)
{
    detectorMessenger = new HadronDetectorMessenger(this);
    stepLimit = new G4UserLimits(maxStep);

    worldMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_Galactic");
    targetMaterial = G4NistManager::Instance()->FindOrBuildMaterial("G4_lAr");

    // maxStep = 0.5*CLHEP::cm;
    // targetRadius = 0.3*CLHEP::cm;
    // targetLength = 4.0*0.5*CLHEP::cm;
    maxStep = 0.5*CLHEP::cm;
    targetRadius = 100*CLHEP::cm;
    targetLength = 10.0*0.5*CLHEP::cm;
}

HadronDetectorConstruction::~HadronDetectorConstruction()
{
    delete stepLimit;
    delete detectorMessenger;
}

G4VPhysicalVolume* HadronDetectorConstruction::Construct()
{
    //std::cout<<"HadronDetectorConstruction::Construct: "<<std::endl;
    //G4NistManager* nistManager=G4NistManager::Instance();
    //G4Material* vacuum = new G4Material("Vacuum",1.0,1.01*g/mole,1.0E-25*g/cm3,kStateGas,2.73*kelvin,3.0E-18*pascal);
    //G4Material* vacuum    = nistManager->FindOrBuildMaterial("G4_Galactic");


    //--------- Definitions of Solids, Logical Volumes, Physical Volumes ---------
    // World

    G4double worldRadius = targetRadius + CLHEP::cm;
    G4double worldLength = 2.0*targetLength + CLHEP::cm;

    //G4GeometryManager::GetInstance()->SetWorldMaximumExtent(worldLength);
    //std::cout<<"construct: world solid"<<std::endl;
    solidWorld = new G4Tubs("world", 0., worldRadius, worldLength, 0., CLHEP::twopi);
    //std::cout<<"construct: world logic"<<std::endl;
    logicWorld = new G4LogicalVolume(solidWorld, worldMaterial, "world", 0, 0, 0);

    //  Must place the World Physical volume unrotated at (0, 0, 0).
    // 
    //std::cout<<"construct: world physical"<<std::endl;
    physicalWorld = new G4PVPlacement(
            0,               // no rotation
            G4ThreeVector(0, 0, 0), // at (0, 0, 0)
            logicWorld,      // its logical volume
            "world",         // its name
            0,               // its mother  volume
            false,           // no boolean operations
            0);              // copy number

    //------------------------------ 
    // Target
    //------------------------------


    //std::cout<<"construct: target material"<<std::endl;
    //G4Material* targetMaterial=nistManager->FindOrBuildMaterial("G4_Fe");
    //G4Material* targetMaterial=nistManager->FindOrBuildMaterial("G4_lAr");
    //G4Material* targetMaterial=nistManager->FindOrBuildMaterial("G4_He");
    //fTargetLength=1*CLHEP::mm;
    //G4double fHalfTargetLength=0.5*fTargetLength;

    //std::cout<<"construct: target solid"<<std::endl;
    solidTarget = new G4Tubs("target", 0., targetRadius, targetLength, 0., CLHEP::twopi);
    //std::cout<<"construct: target logic"<<std::endl;
    //logicTarget = new G4LogicalVolume(solidTarget,vacuum,"Target",0,0,0);
    logicTarget = new G4LogicalVolume(solidTarget, targetMaterial, "target", 0, 0, 0);
    // std::cout << "density: " << targetMaterial->GetDensity()/(CLHEP::g/CLHEP::cm3) << " g/cm^3" << std::endl;
    //std::cout<<"construct: target physical"<<std::endl;
    physicalTarget = new G4PVPlacement(
                0,               // no rotation
                // G4ThreeVector(0, 0, 1),  // at (x, y, z)
                G4ThreeVector(0, 0, targetLength),  // at (x, y, z)
                logicTarget,     // its logical volume
                "target",        // its name
                logicWorld,      // its mother  volume
                false,           // no boolean operations
                0);              // copy number 

    HadronAnalysisManager * analysisManager = HadronAnalysisManager::getInstance();

    analysisManager->SetTargetRadius(solidTarget->GetOuterRadius() / CLHEP::cm);
    analysisManager->SetTargetLength(2.*solidTarget->GetZHalfLength() / CLHEP::cm);

    //------------------------------------------------ 
    // Sensitive detectors
    //------------------------------------------------ 


    //std::cout<<"construct: detector"<<std::endl;
    G4String trackerChamberSDname = "HadronSensitiveDetector";
    HadronSensitiveDetector* aTrackerSD = new HadronSensitiveDetector(trackerChamberSDname);
    //std::cout<<"construct: adding detector to SDM"<<std::endl;
    G4SDManager* sdManager = G4SDManager::GetSDMpointer();
    sdManager->AddNewDetector( aTrackerSD );
    logicTarget->SetSensitiveDetector( aTrackerSD );

    //std::cout<<"construct: setting user steps"<<std::endl;
    //stepLimit = new G4UserLimits(maxStep);
    //stepLimit->SetMaxAllowedStep(maxStep);
    logicTarget->SetUserLimits(stepLimit);

    analysisManager->SetTargetMaxStep(maxStep / CLHEP::cm);
    analysisManager->SetTargetMaterial(targetMaterial);

    //std::cout<<"construct: returning constructed world"<<std::endl;
    return physicalWorld;
}

//-----------------------------------------------------------------------------
void HadronDetectorConstruction::SetWorldMaterial(const G4String& materialString)
{
    // search the material by its name
    G4Material* material = G4NistManager::Instance()->FindOrBuildMaterial(materialString);

    if (material && material != worldMaterial) {
        worldMaterial = material;
        if (logicWorld) logicWorld->SetMaterial(worldMaterial);
        G4RunManager::GetRunManager()->PhysicsHasBeenModified();
    }
}

//-----------------------------------------------------------------------------
void HadronDetectorConstruction::SetTargetMaterial(const G4String& materialString)
{
    // search the material by its name
    G4Material* material = G4NistManager::Instance()->FindOrBuildMaterial(materialString);

    if (material && material != targetMaterial) {
        targetMaterial = material;
        if (logicTarget) logicTarget->SetMaterial(targetMaterial);
        G4RunManager::GetRunManager()->PhysicsHasBeenModified();
    }
}

//-----------------------------------------------------------------------------
void HadronDetectorConstruction::SetTargetRadius(G4double value)
{
    if (value > 0.0) {
        targetRadius = value;
        G4RunManager::GetRunManager()->GeometryHasBeenModified();
    }
}

//-----------------------------------------------------------------------------
void HadronDetectorConstruction::SetTargetLength(G4double value)
{
    if (value > 0.0) {
        targetLength = value*0.5;
        G4RunManager::GetRunManager()->GeometryHasBeenModified();
    }
}

//-----------------------------------------------------------------------------
void HadronDetectorConstruction::SetTargetMaxStep(G4double value)
{
    if (value > 0.0) {
        maxStep = value;
        stepLimit->SetMaxAllowedStep(value);
        // logicTarget->SetUserLimits(stepLimit);
        // G4RunManager::GetRunManager()->GeometryHasBeenModified();
        HadronAnalysisManager * analysisManager = HadronAnalysisManager::getInstance();
        analysisManager->SetTargetMaxStep(value / CLHEP::cm);
    }
}

//-----------------------------------------------------------------------------
void HadronDetectorConstruction::UpdateGeometry()
{
    G4RunManager::GetRunManager()->DefineWorldVolume(Construct());
}

