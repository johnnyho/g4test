#include "HadronPrimaryGeneratorAction.hh"
#include "HadronDetectorConstruction.hh"

#include "HadronAnalysisManager.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"

#include "Randomize.hh"

HadronPrimaryGeneratorAction::HadronPrimaryGeneratorAction( HadronDetectorConstruction* hadronDC) :
    hadronDetector(hadronDC)
{
    //std::cout<<"HadronPrimaryGeneratorAction: creating particle "<<std::endl;
    G4int nParticles = 1;
    particleGun = new G4ParticleGun(nParticles);

    // default particle
    G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
    //G4ParticleDefinition* particle = particleTable->FindParticle("e-");
    G4ParticleDefinition* particle = particleTable->FindParticle("pi-");
    //G4ParticleDefinition* particle = particleTable->FindParticle("gamma"); 
    //G4ParticleDefinition* particle = particleTable->FindParticle("neutron");
    //G4ParticleDefinition* particle = particleTable->FindParticle("alpha");
    //G4ParticleDefinition* particle = particleTable->FindParticle("proton");

    particleGun->SetParticleDefinition(particle);
    particleGun->SetParticleMomentumDirection(G4ThreeVector(0, 0, 1));
    // std::cout<<"HadronPrimaryGeneratorAction: setting position "<<std::endl;
    G4double zVertex = -5.*CLHEP::mm;
    particleGun->SetParticlePosition(G4ThreeVector(0, 0, zVertex));
}

HadronPrimaryGeneratorAction::~HadronPrimaryGeneratorAction()
{
    delete particleGun;
}

void HadronPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{ 
    // std::cout<<"HadronPrimaryGeneratorAction: shooting "<<std::endl;
    // particleGun->SetParticleMomentumDirection(G4ThreeVector(0, 0, 1));
    // particleGun->SetParticleEnergy(200*CLHEP::MeV);
    particleGun->GeneratePrimaryVertex(anEvent);

    HadronAnalysisManager* analysisManager = HadronAnalysisManager::getInstance();
    analysisManager->SetInitialKineticEnergy(particleGun->GetParticleEnergy() / CLHEP::MeV);
    analysisManager->SetParticle(particleGun->GetParticleDefinition());
}

