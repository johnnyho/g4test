#include "HadronAnalysisMessenger.hh"

#include "HadronAnalysisManager.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"

//-----------------------------------------------------------------------------
HadronAnalysisMessenger::HadronAnalysisMessenger(HadronAnalysisManager* analysis) :
    analysisManager(analysis)
{
    analysisDirectory = new G4UIdirectory("/runAnalysis/");
    analysisDirectory->SetGuidance("Hadronronic analysis.");

    outputDirectoryCommand = new G4UIcmdWithAString("/runAnalysis/outputDirectory", this);
    outputDirectoryCommand->SetGuidance("Select ROOT output directory");
    outputDirectoryCommand->SetParameterName("outputDirectory", false);
    outputDirectoryCommand->AvailableForStates(G4State_PreInit, G4State_Idle);

    saveStepsCommand = new G4UIcmdWithABool("/runAnalysis/saveSteps", this);
    saveStepsCommand->SetGuidance("Save steps to ROOT files");
    saveStepsCommand->SetParameterName("saveSteps", false);
    saveStepsCommand->AvailableForStates(G4State_PreInit, G4State_Idle);
}

//-----------------------------------------------------------------------------
HadronAnalysisMessenger::~HadronAnalysisMessenger()
{
    delete outputDirectoryCommand;
    delete analysisDirectory;
}

//-----------------------------------------------------------------------------
void HadronAnalysisMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
    if (command == outputDirectoryCommand) analysisManager->SetOutputDirectory(newValue);
    if (command == saveStepsCommand) analysisManager->SetSaveSteps(saveStepsCommand->GetNewBoolValue(newValue));
}
