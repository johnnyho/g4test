#include "HadronSensitiveDetector.hh"
#include "HadronAnalysisManager.hh"

#include "G4HCofThisEvent.hh"
#include "G4Step.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"

#include "G4VProcess.hh"

#include <algorithm>

HadronSensitiveDetector::HadronSensitiveDetector(G4String name) :
    G4VSensitiveDetector(name)
{
    elasticProcessNames.push_back("hadElastic");

    inelasticProcessNames.push_back("pi-Inelastic");
    inelasticProcessNames.push_back("pi+Inelastic");
    inelasticProcessNames.push_back("protonInelastic");
    inelasticProcessNames.push_back("neutronInelastic");
    inelasticProcessNames.push_back("kaon-Inelastic");
    inelasticProcessNames.push_back("kaon+Inelastic");

    allowedProcessNames.push_back("hIoni");
    allowedProcessNames.push_back("CoulombScat");
    allowedProcessNames.push_back("hBrems");
    allowedProcessNames.push_back("hPairProd");
    allowedProcessNames.push_back("msc");
    allowedProcessNames.push_back("StepLimiter");
    allowedProcessNames.insert(allowedProcessNames.end(), elasticProcessNames.begin(), elasticProcessNames.end());
    allowedProcessNames.insert(allowedProcessNames.end(), inelasticProcessNames.begin(), inelasticProcessNames.end());

    unallowedProcessNames.push_back("Decay");
    unallowedProcessNames.push_back("hBertiniCaptureAtRest");
    unallowedProcessNames.push_back("Transportation");

    processNames.insert(processNames.end(), elasticProcessNames.begin(), elasticProcessNames.end());
    processNames.insert(processNames.end(), inelasticProcessNames.begin(), inelasticProcessNames.end());
    processNames.insert(processNames.end(), allowedProcessNames.begin(), allowedProcessNames.end());
    processNames.insert(processNames.end(), unallowedProcessNames.begin(), unallowedProcessNames.end());
}

HadronSensitiveDetector::~HadronSensitiveDetector()
{}

void HadronSensitiveDetector::Initialize(G4HCofThisEvent* HCE)
{}

G4bool HadronSensitiveDetector::ProcessHits(G4Step* aStep, G4TouchableHistory*)
{
    const G4Track* track = aStep->GetTrack();

    if (track->GetParentID() == 0)
    {
        HadronAnalysisManager* analysisManager = HadronAnalysisManager::getInstance();

        if (analysisManager->GetElastic() or analysisManager->GetInelastic()) return true;

        const G4double energyDeposit = aStep->GetTotalEnergyDeposit();
        const G4double stepLength = aStep->GetStepLength();

        const G4StepPoint* prePoint = aStep->GetPreStepPoint();
        const G4StepPoint* postPoint = aStep->GetPostStepPoint();

        const G4ThreeVector p1 = prePoint->GetPosition();
        const G4ThreeVector p2 = postPoint->GetPosition();

        // double const dE = (postPoint->GetKineticEnergy() - prePoint->GetKineticEnergy()) / CLHEP::MeV;
        // double const dx = (p2-p1).mag() / CLHEP::cm;

        double const dE = energyDeposit / CLHEP::MeV;
        double const dx = stepLength / CLHEP::cm;

        const G4String processName =  postPoint->GetProcessDefinedStep()->GetProcessName();

        if (std::find(processNames.begin(), processNames.end(), processName) == processNames.end())
        {
            G4Exception("HadronSensitiveDetector::ProcessHits",
                        "",
                        FatalException,
                        G4String("      Untracked process " + processName).c_str());
        }

        if (std::find(unallowedProcessNames.begin(), unallowedProcessNames.end(), processName) != unallowedProcessNames.end()) return true;

        // std::cout << processName << "; "
        //           << dx << " cm; "
        //           << prePoint->GetKineticEnergy() / CLHEP::MeV << " MeV; "
        //           << dE/dx << " MeV/cm; "
        //           << p1 / CLHEP::cm << " cm → " << p2 / CLHEP::cm << " cm"
        //           << std::endl;

        if (std::find(elasticProcessNames.begin(), elasticProcessNames.end(), processName) != elasticProcessNames.end()) analysisManager->SetElastic(1);
        if (std::find(inelasticProcessNames.begin(), inelasticProcessNames.end(), processName) != inelasticProcessNames.end()) analysisManager->SetInelastic(1);

        if (analysisManager->GetSaveSteps())
        {
            analysisManager->AddKineticEnergy(prePoint->GetKineticEnergy() / CLHEP::MeV);
            analysisManager->AddEnergyDeposit(dE);
            analysisManager->AddStepSize(dx);
        }
    }

    return true;
}

void HadronSensitiveDetector::EndOfEvent(G4HCofThisEvent*)
{
    // std::cout << "Event end!" << std::endl;
}
