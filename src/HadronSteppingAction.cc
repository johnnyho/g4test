
#include "HadronSteppingAction.hh"
#include "G4SteppingManager.hh"

HadronSteppingAction::HadronSteppingAction()
{}

void HadronSteppingAction::UserSteppingAction(const G4Step* step)
{

    /*
    const G4double energyDeposit = step->GetTotalEnergyDeposit();
    if (energyDeposit <= 0.) return;

    const G4double stepLength = step->GetStepLength();
    const G4Track* track = step->GetTrack();

    if (track->GetParentID() != 0) return;

    const G4StepPoint* prePoint = step->GetPreStepPoint();
    const G4StepPoint* postPoint = step->GetPostStepPoint();

    const G4ThreeVector p1 = prePoint->GetPosition();
    const G4ThreeVector p2 = postPoint->GetPosition();

    double const dE = (postPoint->GetKineticEnergy() - prePoint->GetKineticEnergy()) / CLHEP::MeV;
    double const dx = (p2-p1).mag() / CLHEP::cm;

    std::cout << postPoint->GetProcessDefinedStep()->GetProcessName() << "; "
              << (p2-p1).mag() / CLHEP::cm << " cm; "
              // << stepLength / CLHEP::cm << " cm; "
              << prePoint->GetKineticEnergy() / CLHEP::MeV << " MeV; "
              << -dE/dx << " MeV/cm; "
              << p1 / CLHEP::cm << " cm → " << p2 / CLHEP::cm << " cm"
              // << "; " << energyDeposit / CLHEP::MeV << " MeV"
              // << "; " << -dE << " MeV"
              << std::endl;
    */

}

