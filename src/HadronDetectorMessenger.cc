
#include "HadronDetectorMessenger.hh"

#include "HadronDetectorConstruction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"
#include "G4UIcmdWithoutParameter.hh"

HadronDetectorMessenger::HadronDetectorMessenger(HadronDetectorConstruction * hadronDC) :
    detector(hadronDC)
{
    hadronDirectory = new G4UIdirectory("/runHadron/");
    hadronDirectory->SetGuidance("Hadronic extended example.");

    worldMaterialCommand = new G4UIcmdWithAString("/runHadron/worldMaterial", this);
    worldMaterialCommand->SetGuidance("Select the material for the world");
    worldMaterialCommand->SetParameterName("worldMaterial", false);
    worldMaterialCommand->AvailableForStates(G4State_PreInit, G4State_Idle);

    targetMaterialCommand = new G4UIcmdWithAString("/runHadron/targetMaterial", this);
    targetMaterialCommand->SetGuidance("Select the material for the target");
    targetMaterialCommand->SetParameterName("targetMaterial", false);
    targetMaterialCommand->AvailableForStates(G4State_PreInit, G4State_Idle);

    targetRadiusCommand = new G4UIcmdWithADoubleAndUnit("/runHadron/targetRadius", this);
    targetRadiusCommand->SetGuidance("Set the radius of the target");
    targetRadiusCommand->SetParameterName("targetRadius", false);
    targetRadiusCommand->SetUnitCategory("Length");
    targetRadiusCommand->SetRange("targetRadius>0");
    targetRadiusCommand->AvailableForStates(G4State_PreInit, G4State_Idle);

    targetLengthCommand = new G4UIcmdWithADoubleAndUnit("/runHadron/targetLength", this);
    targetLengthCommand->SetGuidance("Set the length of the target");
    targetLengthCommand->SetParameterName("targetLength", false);
    targetLengthCommand->SetUnitCategory("Length");
    targetLengthCommand->SetRange("targetLength>0");
    targetLengthCommand->AvailableForStates(G4State_PreInit, G4State_Idle);

    targetMaxStepCommand = new G4UIcmdWithADoubleAndUnit("/runHadron/targetMaxStep", this);
    targetMaxStepCommand->SetGuidance("Set the length of the target");
    targetMaxStepCommand->SetParameterName("targetMaxStep", false);
    targetMaxStepCommand->SetUnitCategory("Length");
    targetMaxStepCommand->SetRange("targetMaxStep>0");
    targetMaxStepCommand->AvailableForStates(G4State_PreInit, G4State_Idle);

    updateCommand = new G4UIcmdWithoutParameter("/runHadron/update", this);
    updateCommand->SetGuidance("Update geometry:");
    updateCommand->SetGuidance("This command MUST be applied before \"beamOn\" ");
    updateCommand->SetGuidance("if you changed geometrical value(s)");
    updateCommand->AvailableForStates(G4State_PreInit, G4State_Idle);
}

HadronDetectorMessenger::~HadronDetectorMessenger()
{
    delete worldMaterialCommand;
    delete targetMaterialCommand;
    delete targetRadiusCommand;
    delete targetLengthCommand;
    delete updateCommand;
}

void HadronDetectorMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
    if (command == worldMaterialCommand) detector->SetWorldMaterial(newValue);
    if (command == targetMaterialCommand) detector->SetTargetMaterial(newValue);
    if (command == targetRadiusCommand) detector->SetTargetRadius(targetRadiusCommand->GetNewDoubleValue(newValue));
    if (command == targetLengthCommand) detector->SetTargetLength(targetLengthCommand->GetNewDoubleValue(newValue));
    if (command == targetMaxStepCommand) detector->SetTargetMaxStep(targetMaxStepCommand->GetNewDoubleValue(newValue));
    if (command == updateCommand) detector->UpdateGeometry();
}

