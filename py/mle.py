import sys

import numpy as np
# import arviz as az
import iminuit

n = 2.10446e-2  # LAr, 1 / (cm * b)

def make_nll(interacting, noninteracting, lower_energy_bin_edge, upper_energy_bin_edge, n):

    interacting_counts = interacting[0]
    interacting_x_bin_edges = interacting[1]
    interacting_y_bin_edges = interacting[2]
    interacting_x_bin_centers = 0.5*(interacting_x_bin_edges[1:] + interacting_x_bin_edges[:-1])
    interacting_y_bin_centers = 0.5*(interacting_y_bin_edges[1:] + interacting_y_bin_edges[:-1])

    noninteracting_counts = noninteracting[0]
    noninteracting_x_bin_edges = noninteracting[1]
    noninteracting_y_bin_edges = noninteracting[2]
    noninteracting_x_bin_centers = 0.5*(noninteracting_x_bin_edges[1:] + noninteracting_x_bin_edges[:-1])
    noninteracting_y_bin_centers = 0.5*(noninteracting_y_bin_edges[1:] + noninteracting_y_bin_edges[:-1])

    def nll(x):

        _nll = 0

        for i in range(len(interacting_x_bin_centers)):
            lower_bin_edge = int(interacting_x_bin_edges[i])
            upper_bin_edge = int(interacting_x_bin_edges[i+1])
            if not (lower_bin_edge >= lower_energy_bin_edge and upper_bin_edge <= upper_energy_bin_edge):
                continue
            #print('bin edges: [{}, {})'.format(lower_bin_edge, upper_bin_edge))
            for j in range(len(interacting_y_bin_centers)):
                counts = interacting_counts[i, j]
                #ds = interacting_y_bin_centers[j]
                ds = interacting_y_bin_edges[j]
                #print('  ds:', ds)
                #print('  interacting_counts[{}, {}]:'.format(i, j), interacting_counts[i, j])
                if counts > 0:
                    _nll += -counts*np.log(1-np.exp(-x*n*ds))

        for i in range(len(noninteracting_x_bin_centers)):
            lower_bin_edge = int(noninteracting_x_bin_edges[i])
            upper_bin_edge = int(noninteracting_x_bin_edges[i+1])
            if not (lower_bin_edge >= lower_energy_bin_edge and upper_bin_edge <= upper_energy_bin_edge):
                continue
            for j in range(len(noninteracting_y_bin_centers)):
                counts = noninteracting_counts[i, j]
                #ds = noninteracting_y_bin_centers[j]
                ds = noninteracting_y_bin_edges[j]
                #print('  ds:', ds)
                #print('  noninteracting_counts[{}, {}]:'.format(i, j), noninteracting_counts[i, j])
                if counts > 0:
                    _nll += counts*x*n*ds

        if _nll == 0:
            _nll = np.nan

        return _nll
        #return -np.log(1-np.exp(-x*n*interacting)).sum() + x*n*np.array(noninteracting).sum()

    return nll

def run_minuit(interacting, noninteracting, xs_bin_edges, n=n):

    xs_bin_centers = 0.5*(xs_bin_edges[1:] + xs_bin_edges[:-1])

    # x = xs_bin_centers
    # x_error_lower = np.gradient(x) / 2.0
    # x_error_upper = np.gradient(x) / 2.0

    y = []

    y_error_lower = []
    y_error_upper = []

    for idx in range(len(xs_bin_edges[:-1])):

        lower_bin_edge = xs_bin_edges[idx]
        upper_bin_edge = xs_bin_edges[idx+1]

        try:

            # construct minuit object
            minuit = iminuit.Minuit(
                fcn=make_nll(interacting, noninteracting, lower_bin_edge, upper_bin_edge, n),
                x=1, error_x=1e-5, limit_x=(0, None), errordef=0.5)

            # run migrad minimizer
            minuit.migrad(ncall=1000000)

            # run hesse
            minuit.hesse()

            # print("minuit.values:\n", minuit.values)
            # print("minuit.errors:\n", minuit.errors)

            # run minos
            minuit.minos()

            # print("minuit.get_merrors():\n", minuit.get_merrors())
            # print("minuit.get_merrors():\n", minuit.get_merrors()['x']['lower'], minuit.get_merrors()['x']['upper'])

            y.append(minuit.values['x'])
            y_error_lower.append(-minuit.get_merrors()['x']['lower'])
            y_error_upper.append(minuit.get_merrors()['x']['upper'])

        except:

            y.append(np.nan)
            y_error_lower.append(np.nan)
            y_error_upper.append(np.nan)

    return np.array(y), np.array([ y_error_lower, y_error_upper ])

