import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

import awkward
import uproot

import arviz as az

import calceff2

import progress
import plot

import config

def get_arrays(directory_list):

    interacting = None
    noninteracting = None
    noninteracting_weighted = None
    interacting2d = None
    noninteracting2d = None
    interacting_step_size = None
    noninteracting_step_size = None

    for directory in directory_list:

        if (interacting is None
            or noninteracting is None
            or noninteracting_weighted is None
            or interacting2d is None
            or noninteracting2d is None
            or interacting_step_size is None
            or noninteracting_step_size is None):

            interacting = np.loadtxt(directory+"interacting.txt").astype(int)
            noninteracting = np.loadtxt(directory+"noninteracting.txt").astype(int)
            noninteracting_weighted = np.loadtxt(directory+"noninteracting_weighted.txt")
            interacting2d = np.loadtxt(directory+"interacting2d.txt").astype(int)
            noninteracting2d = np.loadtxt(directory+"noninteracting2d.txt").astype(int)
            interacting_step_size = np.loadtxt(directory+"interacting_step_size.txt")
            noninteracting_step_size = np.loadtxt(directory+"noninteracting_step_size.txt")

        else:

            interacting += np.loadtxt(directory+"interacting.txt").astype(int)
            noninteracting += np.loadtxt(directory+"noninteracting.txt").astype(int)
            noninteracting_weighted += np.loadtxt(directory+"noninteracting_weighted.txt")
            interacting2d += np.loadtxt(directory+"interacting2d.txt").astype(int)
            noninteracting2d += np.loadtxt(directory+"noninteracting2d.txt").astype(int)
            interacting_step_size += np.loadtxt(directory+"interacting_step_size.txt")
            noninteracting_step_size += np.loadtxt(directory+"noninteracting_step_size.txt")

    #print("interacting:")
    #print(interacting)
    #print("noninteracting:")
    #print(noninteracting)
    #print("noninteracting_weighted:")
    #print(noninteracting_weighted)
    #print("interacting2d:")
    #print(interacting2d.sum(axis=1))
    #print("noninteracting2d:")
    #print(noninteracting2d.sum(axis=1))
    #print(noninteracting2d.reshape(200, 820).sum(axis=1))
    #print(noninteracting2d.reshape(40, 5, 820).sum(axis=-1))
    #print(noninteracting2d.reshape(40, 5, 820).sum(axis=-1).sum(axis=1))

    #print(noninteracting.shape, noninteracting2d.shape, noninteracting2d.sum(axis=1).shape)

    #print("interacting2d (step size):")
    #print(interacting2d.reshape(200, 82, 10).sum(axis=0).sum(axis=1))
    #print("noninteracting2d (step size):")
    #print(noninteracting2d.reshape(200, 82, 10).sum(axis=0).sum(axis=1))

    #print("interacting2d + noninteracting2d (step size):")
    #print(interacting2d.reshape(200, 82, 10).sum(axis=0).sum(axis=1) + noninteracting2d.reshape(200, 82, 10).sum(axis=0).sum(axis=1))

    interacting_step_size = interacting2d.reshape(200, 82, 10).sum(axis=0).sum(axis=1)
    noninteracting_step_size = noninteracting2d.reshape(200, 82, 10).sum(axis=0).sum(axis=1)

    #print(interacting_step_size.shape, noninteracting_step_size.shape)

    return {
        "interacting" : interacting,
        "noninteracting" : noninteracting,
        "noninteracting_weighted" : noninteracting_weighted,
        "interacting2d" : interacting2d,
        "noninteracting2d" : noninteracting2d,
        "interacting_step_size" : interacting_step_size,
        "noninteracting_step_size" : noninteracting_step_size,
    }

if __name__ == "__main__":

    # fig = plt.figure(figsize=(7, 5))
    # ax = fig.add_subplot(1, 1, 1)

    # p = ax.imshow(
    #     interacting2d + noninteracting2d,
    #     #np.ma.masked_where(h == 0, h).T,
    #     interpolation='nearest',
    #     origin='low',
    #     extent=[config.x_bin_edges[0], config.x_bin_edges[-1], config.y_bin_edges[0], config.y_bin_edges[-1]],
    #     #vmin=0,
    #     #vmax=10000,
    #     #vmin=vmin,
    #     #vmax=vmax,
    #     #norm=norm,
    # )

    # plt.show()

    # filename = "/Volumes/mnt/work/geant4/" + config.element + "/tfile_" + config.element + "_run_01mm.root"
    filename = "/Users/johnnyho/scratch/g4test/py/g4xs/tfile_" + config.element.split("_")[0] + "_run_01mm.root"
    output_dir = "./tmp/test/"

    f = uproot.open(filename)

    hadron_tree = f["HadronAnalysis"]
    g4xs_tree = f["CrossSection"]

    g4ke = g4xs_tree.array("energy")
    g4xs = g4xs_tree.array("total")

    target_length = []
    interacting_ = []
    noninteracting_ = []

    #

    a_ratio = []
    a_diff = []
    a_fractional_diff = []

    b_ratio = []
    b_diff = []
    b_fractional_diff = []

    c_ratio = []
    c_diff = []
    c_fractional_diff = []

    #

    a_ratio_std = []
    a_diff_std = []
    a_fractional_diff_std = []

    b_ratio_std = []
    b_diff_std = []
    b_fractional_diff_std = []

    c_ratio_std = []
    c_diff_std = []
    c_fractional_diff_std = []

    #

    a_ratio_hdi = []
    a_diff_hdi = []
    a_fractional_diff_hdi = []

    b_ratio_hdi = []
    b_diff_hdi = []
    b_fractional_diff_hdi = []

    c_ratio_hdi = []
    c_diff_hdi = []
    c_fractional_diff_hdi = []

    #

    for idx in range(1, 81):
    #for idx in range(1, 81, 10):
    #for idx in (1, 2, 3, 4, 77, 78, 79, 80):
    #for idx in range(1, 6):

        print("Processing {} mm ...".format(idx))

        directory_list = [ "/Users/johnnyho/scratch/g4test/py/output/" + config.element + "/" + config.element + "_piplus_{}mm/".format(str(idx).zfill(2)) ]

        arr = get_arrays(directory_list)

        interacting = arr["interacting"]
        noninteracting = arr["noninteracting"]
        noninteracting_weighted = arr["noninteracting_weighted"]
        interacting2d = arr["interacting2d"]
        noninteracting2d = arr["noninteracting2d"]
        interacting_step_size = arr["interacting_step_size"]
        noninteracting_step_size = arr["noninteracting_step_size"]

        mean_track_pitch = 0
        #print("mean_track_pitch:", mean_track_pitch)
        mean_track_pitch += np.sum(config.step_size_bin_edges[:-1] * interacting_step_size)
        #print("mean_track_pitch:", mean_track_pitch)
        mean_track_pitch += np.sum(config.step_size_bin_edges[:-1] * noninteracting_step_size)
        #print("mean_track_pitch:", mean_track_pitch)
        mean_track_pitch /= np.sum(interacting_step_size + noninteracting_step_size)
        #print("mean_track_pitch:", mean_track_pitch)

        # import sys
        # sys.exit()

        input_list = [
            {
                'interacting': interacting,
                'noninteracting': noninteracting,
                #'mean_track_pitch': 0.4,
                'mean_track_pitch': mean_track_pitch,
                'n': config.n,
                #'label': 'Monte Carlo (weighted ratio)',
                #'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (ratio)',
                'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (unweighted ratio)',
                'color': 'C0',
            },
        ]

        a = plot.mc_model_ratio(input_list, config.bin_centers, g4ke=g4ke, g4xs=g4xs, arr=True)

        input_list = [
            {
                'interacting': interacting,
                'noninteracting': noninteracting_weighted,
                'mean_track_pitch': 1.0,
                'n': config.n,
                #'label': 'Monte Carlo (weighted ratio)',
                #'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (w.~ratio)',
                'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (weighted ratio)',
                'color': 'C0',
            },
        ]

        b = plot.mc_model_ratio_w(input_list, config.bin_centers, g4ke=g4ke, g4xs=g4xs, arr=True)

        input_list = [
            {
                'interacting2d': (interacting2d, config.x_bin_edges, config.y_bin_edges),
                'noninteracting2d': (noninteracting2d, config.x_bin_edges, config.y_bin_edges),
                'n': config.n,
                #'label': 'Monte Carlo (likelihood)',
                'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (likelihood)',
                'color': 'C0',
            },
        ]

        c = plot.mc_model_mle(input_list, config.bin_edges, g4ke=g4ke, g4xs=g4xs, arr=True)

        #print(a['xs'], b['xs'], c['xs'])
        #print(a['ke'], b['ke'], c['ke'])

        #print(g4xs, g4ke)

        #g4flag = np.in1d(g4ke, a['ke'])
        g4flag = np.isin(g4ke, a['ke'])

        g4xs_ = g4xs[g4flag]

        """
        print('g4xs_:')
        print(g4xs_)
        print('a[\'xs\']:')
        print(a['xs'])
        print('b[\'xs\']:')
        print(b['xs'])
        print('c[\'xs\']:')
        print(c['xs'])
        print('ratio:')
        print('a')
        print(a['xs']/g4xs_, (a['xs']/g4xs_).mean())
        print('b')
        print(b['xs']/g4xs_, (b['xs']/g4xs_).mean())
        print('c')
        print(c['xs']/g4xs_, (c['xs']/g4xs_).mean())
        #print(a['ratio'], b['ratio'], c['ratio'])
        print('difference:')
        print('a')
        print(a['xs']-g4xs_, (a['xs']-g4xs_).mean())
        print('b')
        print(b['xs']-g4xs_, (b['xs']-g4xs_).mean())
        print('c')
        print(c['xs']-g4xs_, (c['xs']-g4xs_).mean())
        print('fractional difference:')
        print('a')
        print((a['xs']-g4xs_)/g4xs_, ((a['xs']-g4xs_)/g4xs_).mean())
        print('b')
        print((b['xs']-g4xs_)/g4xs_, ((b['xs']-g4xs_)/g4xs_).mean())
        print('c')
        print((c['xs']-g4xs_)/g4xs_, ((c['xs']-g4xs_)/g4xs_).mean())
        """

        # plot.histogram1d(interacting_step_size, config.step_size_bin_edges,
        #                  'interacting step size [cm]',
        #                  'entries per {} cm'.format(config.step_size_bin_width),
        #                  savefig=output_dir+'interacting_step_size.pdf')

        # plot.histogram1d(noninteracting_step_size, config.step_size_bin_edges,
        #                  'non-interacting step size [cm]',
        #                  'entries per {} cm'.format(config.step_size_bin_width),
        #                  savefig=output_dir+'noninteracting_step_size.pdf')

        # plot.histogram1d(interacting_step_size+noninteracting_step_size,
        #                  config.step_size_bin_edges,
        #                  'step size [cm]',
        #                  'entries per {} cm'.format(config.step_size_bin_width),
        #                  savefig=output_dir+'step_size.pdf')

        target_length.append(idx)

        start = 2

        hdi_prob = 0.683

        a_r = a['xs'][start:]/g4xs_[start:]
        a_d = a['xs'][start:]-g4xs_[start:]
        a_f = (a['xs'][start:]-g4xs_[start:])/g4xs_[start:]

        a_ratio.append(a_r.mean())
        a_diff.append(a_d.mean())
        a_fractional_diff.append(a_f.mean())

        a_ratio_std.append(np.std(a_r, ddof=1))
        a_diff_std.append(np.std(a_d, ddof=1))
        a_fractional_diff_std.append(np.std(a_f, ddof=1))

        a_ratio_hdi.append(az.hdi(a_r, hdi_prob))
        a_diff_hdi.append(az.hdi(a_d, hdi_prob))
        a_fractional_diff_hdi.append(az.hdi(a_f, hdi_prob))

        b_r = b['xs'][start:]/g4xs_[start:]
        b_d = b['xs'][start:]-g4xs_[start:]
        b_f = (b['xs'][start:]-g4xs_[start:])/g4xs_[start:]

        b_ratio.append(b_r.mean())
        b_diff.append(b_d.mean())
        b_fractional_diff.append(b_f.mean())

        b_ratio_std.append(np.std(b_r, ddof=1))
        b_diff_std.append(np.std(b_d, ddof=1))
        b_fractional_diff_std.append(np.std(b_f, ddof=1))

        b_ratio_hdi.append(az.hdi(b_r, hdi_prob))
        b_diff_hdi.append(az.hdi(b_d, hdi_prob))
        b_fractional_diff_hdi.append(az.hdi(b_f, hdi_prob))

        c_r = c['xs'][start:]/g4xs_[start:]
        c_d = c['xs'][start:]-g4xs_[start:]
        c_f = (c['xs'][start:]-g4xs_[start:])/g4xs_[start:]

        c_ratio.append(c_r.mean())
        c_diff.append(c_d.mean())
        c_fractional_diff.append(c_f.mean())

        c_ratio_std.append(np.std(c_r, ddof=1))
        c_diff_std.append(np.std(c_d, ddof=1))
        c_fractional_diff_std.append(np.std(c_f, ddof=1))

        c_ratio_hdi.append(az.hdi(c_r, hdi_prob))
        c_diff_hdi.append(az.hdi(c_d, hdi_prob))
        c_fractional_diff_hdi.append(az.hdi(c_f, hdi_prob))

        interacting_.append(interacting.sum())
        noninteracting_.append(noninteracting.sum())

    #interacting_ = np.array(interacting_)
    #noninteracting_ = np.array(noninteracting_)
    #print(interacting_)
    #print(noninteracting_)

    def _plot(x, y_arr, y_err_arr, x_label, y_label, labels, colors, markers, hline=0,
              legend=False, legend_font_size=16, legend_loc='upper left',
              text=None, text_x=None, text_y=None,
              xlim=None, ylim=None, logy=None, savefig=None):

        fig = plt.figure(figsize=(7, 5))
        ax = fig.add_subplot(1, 1, 1)

        for idx in range(len(y_arr)):

            y = y_arr[idx]
            y_err = y_err_arr[idx]
            label = labels[idx]
            color = colors[idx]
            marker = markers[idx]

            # print(y)

            #ax.plot(x, y,
            #        label=label,
            #        color=color,
            #        marker=marker,
            #        markersize=3,
            #        markerfacecolor="None",
            #        markeredgecolor=color,
            #        linestyle="None")

            markers_, caps_, bars_ = ax.errorbar(
                x, y, yerr=y_err,
                label=label,
                # capsize=2, capthick=1,
                color=color,
                marker=marker,
                markersize=3,
                markerfacecolor=color,
                markeredgecolor=color,
                linewidth=1,
                alpha=0.75,
                linestyle="None",
            )

            markers_.set_alpha(1)

            # ax.scatter(x, y,
            #            color=color,
            #            marker=marker,
            #            s=3,
            #            facecolor=color,
            #            edgecolor=color)

            data = {
                'x': x,
                'y1': [y_ - e for y_, e in zip(y, y_err)],
                'y2': [y_ + e for y_, e in zip(y, y_err)]}
            plt.fill_between(**data, alpha=0.25, facecolor=color, linewidth=0)

        if legend:
            leg = ax.legend(fontsize=legend_font_size, loc=legend_loc)

        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())

        #ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

        ax.grid(True, which='major', axis='both', color='k', linestyle=':',
                linewidth=1, alpha=0.2)

        ax.set_xlabel(x_label, horizontalalignment='right', x=1.0, fontsize=20)
        ax.set_ylabel(y_label, horizontalalignment='right', y=1.0, fontsize=20)

        ax.yaxis.offsetText.set_fontsize(14)
        ax.tick_params(axis='both', which='major', labelsize=14)

        ax.axhline(y=hline, ls=':', lw=1, c='k', alpha=0.75)

        if xlim:
            ax.set_xlim(xlim)

        if ylim:
            ax.set_ylim(ylim)

        #ax.set_xlim(bin_edges[0], bin_edges[-1])

        #if logy:
        #    ax.set_yscale('log', nonposy='clip')
        #    ax.set_ylim(bottom=0.5)
        #else:
        #    ax.set_ylim(bottom=0)

        if text:
            txt_x, txt_y = 15, -0.125
            if text_x:
                txt_x = text_x
            if text_y:
                txt_y = text_y
            ax.text(txt_x, txt_y, text, fontsize=15,
                    horizontalalignment="center", verticalalignment="center")

        if savefig:
            plt.savefig(savefig, bbox_inches='tight')
        else:
            plt.show()

        plt.cla()
        plt.clf()
        plt.close()

    def plot_(x, y_arr, x_label, y_label, labels, colors, markers, hline=0,
              interacting=None, noninteracting=None,
              legend=False, legend_font_size=14, legend_loc='upper left',
              xlim=None, ylim=None, logy=None, savefig=None):

        fig = plt.figure(figsize=(7, 6))
        gs = fig.add_gridspec(nrows=5, ncols=1)
        ax = fig.add_subplot(gs[0:4, 0])
        ax_ratio = fig.add_subplot(gs[4, 0], sharex=ax)

        for idx in range(len(y_arr)):

            y = y_arr[idx]
            label = labels[idx]
            color = colors[idx]
            marker = markers[idx]

            #print(y)

            ax.plot(x, y,
                    label=label,
                    color=color,
                    marker=marker,
                    markersize=3,
                    markerfacecolor="None",
                    markeredgecolor=color,
                    linestyle="None")

        if interacting is not None and noninteracting is not None:

            confidence_level = 0.683 
            k = interacting
            N = interacting + noninteracting

            mode, low, high = calceff2.effic2_array(k, N, confidence_level)

            y_ratio = mode
            y_ratio_error_low = mode - low
            y_ratio_error_high = high - mode

            y_ratio_error = np.array([ y_ratio_error_low, y_ratio_error_high ]) * 200

            x_error_lower = np.gradient(x) / 2.0
            x_error_upper = np.gradient(x) / 2.0
            x_error = np.array([ x_error_lower, x_error_upper ])

            # print(x_error, y_ratio_error)

            # _ = make_error_boxes(ax_ratio, x, y_ratio, xerror=x_error,
            #                      yerror=y_ratio_error, color=inputs['color'],
            #                      facecolor=inputs['color'], label=inputs['label'])

            _ = plot.make_error_boxes(ax_ratio, x, y_ratio, xerror=x_error,
                                      yerror=y_ratio_error, color='k',
                                      facecolor='k')

        if legend:
            leg = ax.legend(fontsize=legend_font_size, loc=legend_loc)

        ax.xaxis.set_minor_locator(AutoMinorLocator())
        ax.yaxis.set_minor_locator(AutoMinorLocator())

        ax_ratio.xaxis.set_minor_locator(AutoMinorLocator())
        ax_ratio.yaxis.set_minor_locator(AutoMinorLocator())

        #ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

        ax.grid(True, which='both', axis='both', color='k', linestyle=':',
                linewidth=1, alpha=0.2)

        ax_ratio.grid(True, which='both', axis='both', color='k', linestyle=':',
                      linewidth=1, alpha=0.2)

        ax.set_xlabel(x_label, horizontalalignment='right', x=1.0, fontsize=18)
        ax.set_ylabel(y_label, horizontalalignment='right', y=1.0, fontsize=18)

        ax.yaxis.offsetText.set_fontsize(14)
        ax.tick_params(axis='both', which='major', labelsize=14)
        ax_ratio.tick_params(axis='both', which='major', labelsize=14)

        ax.axhline(y=hline, ls=':', lw=1, c='k', alpha=0.75)

        ax_ratio.set_ylabel('int./inc.', horizontalalignment='center', fontsize=18)
        ax_ratio.set_xlabel(x_label, horizontalalignment='right', x=1.0, fontsize=18)
        ax.set_ylabel(y_label, horizontalalignment='right', y=1.0, fontsize=18)

        #ax_ratio.set_ylim((0.76, 1.24))
        plt.setp(ax.get_xticklabels(), visible=False)

        if xlim:
            ax.set_xlim(xlim)

        if ylim:
            ax.set_ylim(ylim)

        #ax.set_xlim(bin_edges[0], bin_edges[-1])

        #if logy:
        #    ax.set_yscale('log', nonposy='clip')
        #    ax.set_ylim(bottom=0.5)
        #else:
        #    ax.set_ylim(bottom=0)

        if savefig:
            plt.savefig(savefig, bbox_inches='tight')
        else:
            plt.show()

        plt.cla()
        plt.clf()
        plt.close()

    np.savetxt(output_dir+'target_length.txt', target_length)

    np.savetxt(output_dir+'a_ratio.txt', a_ratio)
    np.savetxt(output_dir+'a_diff.txt', a_diff)
    np.savetxt(output_dir+'a_fractional_diff.txt', a_fractional_diff)

    np.savetxt(output_dir+'a_ratio_std.txt', a_ratio_std)
    np.savetxt(output_dir+'a_diff_std.txt', a_diff_std)
    np.savetxt(output_dir+'a_fractional_diff_std.txt', a_fractional_diff_std)

    np.savetxt(output_dir+'a_ratio_hdi.txt', a_ratio_hdi)
    np.savetxt(output_dir+'a_diff_hdi.txt', a_diff_hdi)
    np.savetxt(output_dir+'a_fractional_diff_hdi.txt', a_fractional_diff_hdi)

    np.savetxt(output_dir+'b_ratio.txt', b_ratio)
    np.savetxt(output_dir+'b_diff.txt', b_diff)
    np.savetxt(output_dir+'b_fractional_diff.txt', b_fractional_diff)

    np.savetxt(output_dir+'b_ratio_std.txt', b_ratio_std)
    np.savetxt(output_dir+'b_diff_std.txt', b_diff_std)
    np.savetxt(output_dir+'b_fractional_diff_std.txt', b_fractional_diff_std)

    np.savetxt(output_dir+'b_ratio_hdi.txt', b_ratio_hdi)
    np.savetxt(output_dir+'b_diff_hdi.txt', b_diff_hdi)
    np.savetxt(output_dir+'b_fractional_diff_hdi.txt', b_fractional_diff_hdi)

    np.savetxt(output_dir+'c_ratio.txt', c_ratio)
    np.savetxt(output_dir+'c_diff.txt', c_diff)
    np.savetxt(output_dir+'c_fractional_diff.txt', c_fractional_diff)

    np.savetxt(output_dir+'c_ratio_std.txt', c_ratio_std)
    np.savetxt(output_dir+'c_diff_std.txt', c_diff_std)
    np.savetxt(output_dir+'c_fractional_diff_std.txt', c_fractional_diff_std)

    np.savetxt(output_dir+'c_ratio_hdi.txt', c_ratio_hdi)
    np.savetxt(output_dir+'c_diff_hdi.txt', c_diff_hdi)
    np.savetxt(output_dir+'c_fractional_diff_hdi.txt', c_fractional_diff_hdi)

    interacting_ = np.array(interacting_)
    noninteracting_ = np.array(noninteracting_)

    _plot(target_length,
          [ a_ratio, b_ratio, c_ratio ],
          [ a_ratio_std, b_ratio_std, c_ratio_std ],
          "target thickness [mm]", "ratio", hline=1,
          #interacting=interacting_, noninteracting=noninteracting_,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=config.y_lim_ratio,
          text=config.text, text_x=15, text_y=1.-0.125,
          savefig=output_dir+"ratio.pdf")

    _plot(target_length,
          [ a_diff, b_diff, c_diff ],
          [ a_diff_std, b_diff_std, c_diff_std ],
          "target thickness [mm]", "difference [b]", hline=0,
          #interacting=interacting_, noninteracting=noninteracting_,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=config.y_lim_diff,
          text=config.text, text_x=15, text_y=-0.3,
          savefig=output_dir+"difference.pdf")

    _plot(target_length,
          [ a_fractional_diff, b_fractional_diff, c_fractional_diff ],
          [ a_fractional_diff_std, b_fractional_diff_std, c_fractional_diff_std ],
          "target thickness [mm]", "fractional difference", hline=0,
          #interacting=interacting_, noninteracting=noninteracting_,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=config.y_lim_fraction_diff,
          text=config.text, text_x=15, text_y=-0.125,
          savefig=output_dir+"fractional_difference.pdf")

    _plot(target_length,
          [ a_diff, b_diff, c_diff ],
          [ a_diff_std, b_diff_std, c_diff_std ],
          "target thickness [mm]", "absolute error [b]", hline=0,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=config.y_lim_diff,
          text=config.text, text_x=15, text_y=-0.3,
          savefig=output_dir+"absolute_error.pdf")

    _plot(target_length,
          [ a_fractional_diff, b_fractional_diff, c_fractional_diff ],
          [ a_fractional_diff_std, b_fractional_diff_std, c_fractional_diff_std ],
          "target thickness [mm]", "relative error", hline=0,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=config.y_lim_fraction_diff,
          text=config.text, text_x=15, text_y=-0.125,
          savefig=output_dir+"relative_error.pdf")

    _plot(target_length,
          np.array([ a_fractional_diff, b_fractional_diff, c_fractional_diff ])*100,
          np.array([ a_fractional_diff_std, b_fractional_diff_std, c_fractional_diff_std ])*100,
          "target thickness [mm]", "relative error [\%]", hline=0,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=(-20, 20),
          text=config.text, text_x=15, text_y=-12.5,
          savefig=output_dir+"relative_error_percent.pdf")

    _plot(target_length,
          np.array([ a_fractional_diff, b_fractional_diff, c_fractional_diff ])*100,
          np.array([ a_fractional_diff_std, b_fractional_diff_std, c_fractional_diff_std ])*100,
          "target thickness [mm]", "percent error [\%]", hline=0,
          labels=["simple ratio", "weighted ratio", "likelihood"],
          colors=["r", "g", "b"], markers=["s", "D", "o"], legend=True,
          xlim=(0, 81), ylim=(-20, 20),
          text=config.text, text_x=15, text_y=-12.5,
          savefig=output_dir+"percent_error.pdf")

