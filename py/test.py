import numpy as np
import awkward

np.random.seed(31)

n = 100

max_step = 10

x = np.arange(n)

step_sizes = np.random.randint(1, max_step+1, size=n)
indices = np.cumsum(step_sizes)
flag = indices < n
indices = indices[flag]

print(step_sizes)
print(step_sizes[flag])
print(np.cumsum(step_sizes)[flag])

y = np.split(x, indices)
z = awkward.fromiter(y)

print(x)
print(y)
print(z)
print(z[:, 0])
print(z.sum())

print((n-indices)[::-1])
