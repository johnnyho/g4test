import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.collections import PatchCollection
from matplotlib.patches import Rectangle
import matplotlib.gridspec as gridspec
import matplotlib.colors as colors
from matplotlib.ticker import AutoMinorLocator, MultipleLocator

from matplotlib import rc
rc('font',**{'family':'serif','serif':['Computer Modern Roman']})
rc('text', usetex=True)

from mle import run_minuit
import calceff2

xs_x_label = 'kinetic energy [MeV]'
xs_y_label = 'cross section [b]'

# g4 = np.loadtxt('/Users/johnnyho/scratch/analysis/g4xs/g4xs_negative_pion_argon_FTFP_BERT.out', skiprows=6, max_rows=2001)
# g4ke = g4[:, 1]
# g4xs = g4[:, 4]
g4ke = np.zeros(2000)
g4xs = np.zeros(2000)
# g4label = '\\textsc{Geant}4 Bertini cascade model'
# g4label = '\\textsc{Geant}4 model'
g4label = 'true cross section from \\textsc{Geant}4'

def make_error_boxes(
        ax, xdata, ydata, xerror, yerror, marker='.', color='k',
        facecolor='g', edgecolor='None', alpha=0.5, markeralpha=1.0,
        label=None):

    # Create list for all the error patches
    errorboxes = []

    # Loop over data points; create box from errors at each point
    for x, y, xe, ye in zip(xdata, ydata, xerror.T, yerror.T):
        rect = Rectangle((x - xe[0], y - ye[0]), xe.sum(), ye.sum())
        errorboxes.append(rect)

    # Create patch collection with specified colour/alpha
    pc = PatchCollection(errorboxes, facecolor=facecolor, alpha=alpha,
                         edgecolor=edgecolor)

    # Add collection to axes
    ax.add_collection(pc)

    # Plot errorbars
    artists = ax.errorbar(xdata, ydata, xerr=xerror, yerr=None,
                          marker=marker, c=color, ls='', ms=3,
                          alpha=markeralpha, label=label)

    return artists

def mc_model_mle(inputs_list, xs_bin_edges, g4ke=g4ke, g4xs=g4xs, savefig=None, arr=False):

    fig = plt.figure(figsize=(7, 6))
    gs = fig.add_gridspec(nrows=5, ncols=1)
    ax_xs = fig.add_subplot(gs[0:4, 0])
    ax_ratio = fig.add_subplot(gs[4, 0], sharex=ax_xs)

    xs_bin_centers = 0.5*(xs_bin_edges[1:] + xs_bin_edges[:-1])
    x = xs_bin_centers
    x_error_lower = np.gradient(x) / 2.0
    x_error_upper = np.gradient(x) / 2.0
    x_error = np.array([ x_error_lower, x_error_upper ])

    # cross section plot

    ax_xs.plot(g4ke, g4xs, color='k', linestyle='dotted', linewidth=2, label=g4label)

    # ratio plot

    flag = np.isin(g4ke, x)

    for inputs in inputs_list:

        # print(inputs)

        y, y_error = run_minuit(inputs['interacting2d'],
                                inputs['noninteracting2d'],
                                xs_bin_edges, inputs['n'])

        #y[0] = np.nan
        #y[-1] = np.nan

        _ = make_error_boxes(ax_xs, x, y, xerror=x_error, yerror=y_error,
                             color=inputs['color'],
                             facecolor=inputs['color'],
                             label=inputs['label'])

        # print('y:\n', y)
        # print('y_error\n:', y_error)

        # ratio plot

        y_ratio = y / g4xs[flag] - 1
        y_ratio_error = y_error / g4xs[flag]

        _ = make_error_boxes(ax_ratio, x, y_ratio*100, xerror=x_error,
                             yerror=y_ratio_error*100, color=inputs['color'],
                             facecolor=inputs['color'], label=inputs['label'])

    leg = ax_xs.legend(fontsize=16, loc='upper right')

    ax_xs.grid(True, which='both', axis='both', color='k', linestyle=':',
               linewidth=1, alpha=0.2)

    ax_ratio.grid(True, which='both', axis='both', color='k', linestyle=':',
                  linewidth=1, alpha=0.2)

    #ax_xs.set_xlim((-60.0, 1260.0))
    #ax_xs.set_xlim((-60.0, 1460.0))
    #ax_xs.set_xlim((-100.0, 2100.0))
    #ax_xs.set_xlim((-50.0, 2050.0))
    ax_xs.set_xlim((0.0, 2000.0))
    ax_xs.set_ylim((0.0, 2.0))
    ax_xs.set_ylim((0.0, 4.0))

    #ax_ratio.set_xlabel(xs_x_label, position=(1., 0.), ha='right', fontsize=18)
    ##ax_xs.set_xlabel(xs_x_label, position=(1., 0.), ha='right', fontsize=14)
    #ax_xs.set_ylabel(xs_y_label, position=(0., 1.), va='top', ha='right', fontsize=18)
    #ax_xs.yaxis.set_label_coords(-0.13, 1.)

    ax_ratio.set_ylabel('rel.\ err.\ [\%]', horizontalalignment='center', fontsize=18)
    ax_ratio.set_xlabel(xs_x_label, horizontalalignment='right', x=1.0, fontsize=18)
    ax_xs.set_ylabel(xs_y_label, horizontalalignment='right', y=1.0, fontsize=18)

    ax_ratio.axhline(y=1, ls=':', lw=1, c='k', alpha=0.75)
    #ax_ratio.set_ylim((0.25, 1.75))
    #ax_ratio.set_ylim((0.9, 1.1))
    #ax_ratio.set_ylim((0.88, 1.12))
    #ax_ratio.set_ylim((0.80, 1.20))
    #ax_ratio.set_ylim((0.76, 1.24))
    ax_ratio.set_ylim((-24.0, 24.0))
    ax_ratio.yaxis.set_minor_locator(AutoMinorLocator())

    plt.setp(ax_xs.get_xticklabels(), visible=False)

    ax_xs.tick_params(axis='both', which='major', labelsize=14)
    ax_ratio.tick_params(axis='both', which='major', labelsize=14)

    if arr:
        plt.cla()
        plt.clf()
        plt.close()
        return { 'xs': y, 'ke': x, 'ratio': y_ratio }

    if savefig:
        plt.savefig(savefig, bbox_inches='tight')
    else:
        plt.show()

    plt.cla()
    plt.clf()
    plt.close()

    return

def mc_model_ratio_w(inputs_list, x, confidence_level=0.683, g4ke=g4ke, g4xs=g4xs, savefig=None, arr=False):

    fig = plt.figure(figsize=(7, 6))
    gs = fig.add_gridspec(nrows=5, ncols=1)
    ax_xs = fig.add_subplot(gs[0:4, 0])
    ax_ratio = fig.add_subplot(gs[4, 0], sharex=ax_xs)

    #x = xs_bin_centers
    x_error_low = np.gradient(x) / 2.0
    x_error_high = np.gradient(x) / 2.0
    x_error = np.array([ x_error_low, x_error_high ])

    # cross section plot

    ax_xs.plot(g4ke, g4xs, color='k', linestyle='dotted', linewidth=2, label=g4label)

    # ratio plot

    flag = np.isin(g4ke, x)

    for inputs in inputs_list:

        k = inputs['interacting']
        N = inputs['noninteracting']

        if k is None or N is None:
            continue

        mode, low, high = calceff2.effic2_array(k, N, confidence_level)

        y = mode
        y_error_low = mode - low
        y_error_high = high - mode

        y_error = np.array([ y_error_low, y_error_high ])

        #y[0] = np.nan
        #y[-1] = np.nan

        y /= inputs['n'] * inputs['mean_track_pitch']
        y_error /= inputs['n'] * inputs['mean_track_pitch']

        _ = make_error_boxes(ax_xs, x, y, xerror=x_error, yerror=y_error,
                             color=inputs['color'],
                             facecolor=inputs['color'],
                             label=inputs['label'])

        # ratio plot

        # y_ratio = y / g4xs[flag]
        # y_ratio_error = y_error / g4xs[flag]

        # _ = make_error_boxes(ax_ratio, x, y_ratio, xerror=x_error,
        #                      yerror=y_ratio_error, color=inputs['color'],
        #                      facecolor=inputs['color'], label=inputs['label'])

        # for expanded x axis

        y_ratio = y[x > 0] / g4xs[flag] - 1
        y_ratio_error = y_error[:, x > 0] / g4xs[flag]

        _ = make_error_boxes(ax_ratio, x[x > 0], y_ratio*100, xerror=x_error[:, x > 0],
                             yerror=y_ratio_error*100, color=inputs['color'],
                             facecolor=inputs['color'], label=inputs['label'])

        """
        # statistically acceptable range

        n_dx = inputs['n'] * inputs['mean_track_pitch']

        y_range = None
        range_flag = None

        with np.errstate(divide='ignore', invalid='ignore'):
            y_range = np.sqrt((g4xs[flag] * n_dx * (1 - g4xs[flag] * n_dx)) / N)
            range_flag = np.isfinite(y_range)
            y_range = np.array([ y_range, y_range ]) / n_dx

        _ = make_error_boxes(ax_xs, x[range_flag], g4xs[flag][range_flag],
                             xerror=x_error[:, range_flag],
                             yerror=y_range[:, range_flag],
                             color='k', facecolor='k', alpha=0.5, markeralpha=0.5)
        """


    leg = ax_xs.legend(fontsize=16, loc='upper right')

    ax_xs.grid(True, which='both', axis='both', color='k', linestyle=':',
               linewidth=1, alpha=0.2)

    ax_ratio.grid(True, which='both', axis='both', color='k', linestyle=':',
                  linewidth=1, alpha=0.2)

    #ax_xs.set_xlim((-60.0, 1260.0))
    #ax_xs.set_xlim((-60.0, 1460.0))
    #ax_xs.set_xlim((-100.0, 2100.0))
    #ax_xs.set_xlim((-50.0, 2050.0))
    ax_xs.set_xlim((0.0, 2000.0))
    ax_xs.set_ylim((0.0, 2.0))
    ax_xs.set_ylim((0.0, 4.0))

    #ax_ratio.set_xlabel(xs_x_label, position=(1., 0.), ha='right', fontsize=18)
    ##ax_xs.set_xlabel(xs_x_label, position=(1., 0.), ha='right', fontsize=14)
    #ax_xs.set_ylabel(xs_y_label, position=(0., 1.), va='top', ha='right', fontsize=18)
    #ax_xs.yaxis.set_label_coords(-0.13, 1.)

    ax_ratio.set_ylabel('rel.\ err.\ [\%]', horizontalalignment='center', fontsize=18)
    ax_ratio.set_xlabel(xs_x_label, horizontalalignment='right', x=1.0, fontsize=18)
    ax_xs.set_ylabel(xs_y_label, horizontalalignment='right', y=1.0, fontsize=18)

    ax_ratio.axhline(y=1, ls=':', lw=1, c='k', alpha=0.75)
    #ax_ratio.set_ylim((0.25, 1.75))
    #ax_ratio.set_ylim((0.9, 1.1))
    #ax_ratio.set_ylim((0.88, 1.12))
    #ax_ratio.set_ylim((0.80, 1.20))
    #ax_ratio.set_ylim((0.76, 1.24))
    ax_ratio.set_ylim((-24.0, 24.0))
    ax_ratio.yaxis.set_minor_locator(AutoMinorLocator())

    plt.setp(ax_xs.get_xticklabels(), visible=False)

    ax_xs.tick_params(axis='both', which='major', labelsize=14)
    ax_ratio.tick_params(axis='both', which='major', labelsize=14)

    if arr:
        plt.cla()
        plt.clf()
        plt.close()
        return { 'xs': y, 'ke': x, 'ratio': y_ratio }

    if savefig:
        plt.savefig(savefig, bbox_inches='tight')
    else:
        plt.show()

    plt.cla()
    plt.clf()
    plt.close()

    return

def mc_model_ratio(inputs_list, x, confidence_level=0.683, g4ke=g4ke, g4xs=g4xs, savefig=None, arr=False):

    fig = plt.figure(figsize=(7, 6))
    gs = fig.add_gridspec(nrows=5, ncols=1)
    ax_xs = fig.add_subplot(gs[0:4, 0])
    ax_ratio = fig.add_subplot(gs[4, 0], sharex=ax_xs)

    #x = xs_bin_centers
    x_error_low = np.gradient(x) / 2.0
    x_error_high = np.gradient(x) / 2.0
    x_error = np.array([ x_error_low, x_error_high ])

    # cross section plot

    ax_xs.plot(g4ke, g4xs, color='k', linestyle='dotted', linewidth=2, label=g4label)

    # ratio plot

    flag = np.isin(g4ke, x)

    for inputs in inputs_list:

        k = inputs['interacting']
        N = inputs['noninteracting'] + k

        if k is None or N is None:
            continue

        mode, low, high = calceff2.effic2_array(k, N, confidence_level)

        y = mode
        y_error_low = mode - low
        y_error_high = high - mode

        y_error = np.array([ y_error_low, y_error_high ])

        #y[0] = np.nan
        #y[-1] = np.nan

        y /= inputs['n'] * inputs['mean_track_pitch']
        y_error /= inputs['n'] * inputs['mean_track_pitch']

        _ = make_error_boxes(ax_xs, x, y, xerror=x_error, yerror=y_error,
                             color=inputs['color'],
                             facecolor=inputs['color'],
                             label=inputs['label'])

        # ratio plot

        # y_ratio = y / g4xs[flag]
        # y_ratio_error = y_error / g4xs[flag]

        # _ = make_error_boxes(ax_ratio, x, y_ratio, xerror=x_error,
        #                      yerror=y_ratio_error, color=inputs['color'],
        #                      facecolor=inputs['color'], label=inputs['label'])

        # for expanded x axis

        y_ratio = y[x > 0] / g4xs[flag] - 1
        y_ratio_error = y_error[:, x > 0] / g4xs[flag]

        _ = make_error_boxes(ax_ratio, x[x > 0], y_ratio*100, xerror=x_error[:, x > 0],
                             yerror=y_ratio_error*100, color=inputs['color'],
                             facecolor=inputs['color'], label=inputs['label'])

        """
        # statistically acceptable range

        n_dx = inputs['n'] * inputs['mean_track_pitch']

        y_range = None
        range_flag = None

        with np.errstate(divide='ignore', invalid='ignore'):
            y_range = np.sqrt((g4xs[flag] * n_dx * (1 - g4xs[flag] * n_dx)) / N)
            range_flag = np.isfinite(y_range)
            y_range = np.array([ y_range, y_range ]) / n_dx

        _ = make_error_boxes(ax_xs, x[range_flag], g4xs[flag][range_flag],
                             xerror=x_error[:, range_flag],
                             yerror=y_range[:, range_flag],
                             color='k', facecolor='k', alpha=0.5, markeralpha=0.5)
        """


    leg = ax_xs.legend(fontsize=16, loc='upper right')

    ax_xs.grid(True, which='both', axis='both', color='k', linestyle=':',
               linewidth=1, alpha=0.2)

    ax_ratio.grid(True, which='both', axis='both', color='k', linestyle=':',
                  linewidth=1, alpha=0.2)

    #ax_xs.set_xlim((-60.0, 1260.0))
    #ax_xs.set_xlim((-60.0, 1460.0))
    #ax_xs.set_xlim((-100.0, 2100.0))
    #ax_xs.set_xlim((-50.0, 2050.0))
    ax_xs.set_xlim((0.0, 2000.0))
    ax_xs.set_ylim((0.0, 2.0))
    ax_xs.set_ylim((0.0, 4.0))

    #ax_ratio.set_xlabel(xs_x_label, position=(1., 0.), ha='right', fontsize=18)
    ##ax_xs.set_xlabel(xs_x_label, position=(1., 0.), ha='right', fontsize=14)
    #ax_xs.set_ylabel(xs_y_label, position=(0., 1.), va='top', ha='right', fontsize=18)
    #ax_xs.yaxis.set_label_coords(-0.13, 1.)

    ax_ratio.set_ylabel('rel.\ err.\ [\%]', horizontalalignment='center', fontsize=18)
    ax_ratio.set_xlabel(xs_x_label, horizontalalignment='right', x=1.0, fontsize=18)
    ax_xs.set_ylabel(xs_y_label, horizontalalignment='right', y=1.0, fontsize=18)

    ax_ratio.axhline(y=1, ls=':', lw=1, c='k', alpha=0.75)
    #ax_ratio.set_ylim((0.25, 1.75))
    #ax_ratio.set_ylim((0.9, 1.1))
    #ax_ratio.set_ylim((0.88, 1.12))
    #ax_ratio.set_ylim((0.80, 1.20))
    #ax_ratio.set_ylim((0.76, 1.24))
    ax_ratio.set_ylim((-24.0, 24.0))
    ax_ratio.yaxis.set_minor_locator(AutoMinorLocator())

    plt.setp(ax_xs.get_xticklabels(), visible=False)

    ax_xs.tick_params(axis='both', which='major', labelsize=14)
    ax_ratio.tick_params(axis='both', which='major', labelsize=14)

    if arr:
        plt.cla()
        plt.clf()
        plt.close()
        return { 'xs': y, 'ke': x, 'ratio': y_ratio }

    if savefig:
        plt.savefig(savefig, bbox_inches='tight')
    else:
        plt.show()

    plt.cla()
    plt.clf()
    plt.close()

    return

def histogram1d(counts, bin_edges, x_label, y_label, color='C0',
                legend=False, legend_font_size=14, legend_loc='upper right',
                density=None, logy=None, savefig=None):

    fig = plt.figure(figsize=(7, 5))
    ax = fig.add_subplot(1, 1, 1)

    bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])

    ax.hist(bin_centers, bins=bin_edges, weights=counts, color=color,
            alpha=1.0, histtype='stepfilled', density=density)

    if legend:
        leg = ax.legend(fontsize=legend_font_size, loc=legend_loc)

    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.yaxis.set_minor_locator(AutoMinorLocator())

    ax.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))

    ax.grid(True, which='both', axis='both', color='k', linestyle=':',
            linewidth=1, alpha=0.2)

    ax.set_xlabel(x_label, horizontalalignment='right', x=1.0, fontsize=18)
    ax.set_ylabel(y_label, horizontalalignment='right', y=1.0, fontsize=18)

    ax.yaxis.offsetText.set_fontsize(14)
    ax.tick_params(axis='both', which='major', labelsize=14)

    ax.set_xlim(bin_edges[0], bin_edges[-1])

    if logy:
        ax.set_yscale('log', nonposy='clip')
        ax.set_ylim(bottom=0.5)
    else:
        ax.set_ylim(bottom=0)

    if savefig:
        plt.savefig(savefig, bbox_inches='tight')
    else:
        plt.show()

    plt.cla()
    plt.clf()
    plt.close()

