#!/usr/bin/env python

import os
import sys
import argparse

import numpy as np
import awkward
import uproot

import progress
import plot

parser = argparse.ArgumentParser(description="analyze cross section")
#parser.add_argument("thickness", type=int, nargs="+", help="target thickness")
parser.add_argument("target", type=str, default=None,
                    help="target (lAr, lXe, lKr, Pb, Sn, ...)")
parser.add_argument("thickness", type=int, default=None,
                    help="thickness of target [mm]")

args = parser.parse_args()

target = str(args.target).lower()
thickness = str(args.thickness).zfill(2)

filename = '/n/holystore01/LABS/guenette_lab/Users/jh/xs-mle/geant4/' + target + '/tfile_' + target + '_run_' + thickness + 'mm.root'
output_dir = '/n/holystore01/LABS/guenette_lab/Users/jh/xs-mle/output/' + target + '/' + target + '_piplus_' + thickness + 'mm/'

filename = '/Volumes/seagate/work/geant4/' + target + '/tfile_' + target + '_run_' + thickness + 'mm.root'
output_dir = '/Users/johnnyho/scratch/g4test/py/output/' + target + '/' + target + '_piplus_' + thickness + 'mm/'

os.mkdir(output_dir)

np.random.seed(31)

single_target = True

adjust_steps = False
#max_step_size = 1.0 * 1  # cm
max_step_size = 1.2 * 1  # cm
max_step = 2 * 10 * 1
#max_step_size = 12.0  # cm
#max_step = 30 * 4
max_step_size = 4.2
max_step = 80

max_step_size = 8.2

n_dict = {
    'lar' : 2.10446e-2,  # LAr, 1 / (cm * b)
    'lkr' : 1.73769e-2,  # LKr, 1 / (cm * b)
    'lxe' : 1.35448e-2,  # LXe, 1 / (cm * b)
     'sn' : 3.70835e-2,  #  Sn, 1 / (cm * b)
     'pb' : 3.29881e-2,  #  Pb, 1 / (cm * b)
}

target = target.split("_")[0]
n = n_dict[target]

bin_lower = 0.0  # MeV
bin_upper = 2000.0  # MeV
bin_width = 50  # MeV
#bin_edges = np.arange(bin_lower, bin_upper+bin_width, bin_width)
bins = np.round((bin_upper-bin_lower)/bin_width).astype(int) + 1
bin_edges = np.linspace(bin_lower, bin_upper, bins)
bin_centers = 0.5*(bin_edges[1:] + bin_edges[:-1])

empty = np.histogram([], bins=bin_edges)[0]

x_bin_lower = bin_lower  # MeV
x_bin_upper = bin_upper  # MeV
x_bin_width = 10  # MeV
x_bins = np.round((x_bin_upper-x_bin_lower)/x_bin_width).astype(int) + 1
x_bin_edges = np.linspace(x_bin_lower, x_bin_upper, x_bins)
x_bin_centers = 0.5*(x_bin_edges[1:] + x_bin_edges[:-1])

y_bin_lower = 0.0  # cm
y_bin_upper = max_step_size  # cm
y_bin_width = 0.01  # cm
y_bins = np.round((y_bin_upper-y_bin_lower)/y_bin_width).astype(int) + 1
y_bin_edges = np.linspace(y_bin_lower, y_bin_upper, y_bins)
y_bin_edges = np.around(y_bin_edges, decimals=2)
y_bin_centers = 0.5*(y_bin_edges[1:] + y_bin_edges[:-1])

empty2d = np.histogram2d([], [], bins=(x_bin_edges, y_bin_edges))[0]

step_size_bin_lower = 0.0  # cm
step_size_bin_upper = max_step_size  # cm
step_size_bin_width = 0.1  # cm
step_size_bins = np.round((step_size_bin_upper-step_size_bin_lower)/step_size_bin_width).astype(int) + 1
step_size_bin_edges = np.linspace(step_size_bin_lower, step_size_bin_upper, step_size_bins)
step_size_bin_edges = np.around(step_size_bin_edges, decimals=1)
step_size_bin_centers = 0.5*(step_size_bin_edges[1:] + step_size_bin_edges[:-1])

empty_step_size_histogram = np.histogram([], bins=step_size_bin_edges)[0]

def run(tree, entry_start=None, entry_stop=None, entry_steps=None):
    """Read TTree and analyze data.

    Parameters
    ----------
    tree : uproot.rootio.TTree
        TTree containing the data.
    entry_start : int, optional
        Starting TTree entry index.
    entry_stop : int, optional
        Stopping TTree entry index.
    entry_steps : int, optional
        Iterate through TTree in steps of equal numbers of entries (except at
        the end of the file)

    Returns
    -------
    output : dict
    
    """

    number_entries_ = tree.numentries
    entry_start_ = 0
    entry_stop_ = tree.numentries

    if isinstance(entry_start, int):
        entry_start_ = entry_start
    if isinstance(entry_stop, int):
        entry_stop_ = entry_stop

    if entry_start_ > entry_stop_ or entry_start_ >= tree.numentries or entry_stop_ > tree.numentries:
        raise ValueError('entry_start or entry_stop out of range')

    number_entries_ = entry_stop_ - entry_start_

    branch_list = [
        'run',
        'event',
        'initialKineticEnergy',
        'targetRadius',
        'targetLength',
        'targetMaxStep',
        'elastic',
        'inelastic',
        'kineticEnergy',
        'energyDeposit',
        'stepSize',
    ]

    # bookkeeping
    count = 0

    interacting = empty.copy()
    noninteracting = empty.copy()
    noninteracting_weighted = empty.copy().astype(float)

    interacting2d = empty2d.copy()
    noninteracting2d = empty2d.copy()

    interacting_step_size = empty_step_size_histogram.copy()
    noninteracting_step_size = empty_step_size_histogram.copy()

    # iterate over tree
    for arrays in tree.iterate(branches=branch_list, entrysteps=entry_steps, entrystart=entry_start_, entrystop=entry_stop_, namedecode='utf-8'):

        steps = len(arrays['run'])

        run_ = arrays['run']
        event_ = arrays['event']

        elastic_ = arrays['elastic']
        inelastic_ = arrays['inelastic']
        kinetic_energy_ = arrays['kineticEnergy']
        step_size_ = arrays['stepSize']

        if single_target:

            # progress bar
            count += steps
            progress.bar((count) / number_entries_)

            interacting_flag = (elastic_ + inelastic_).astype(bool)

            interacting_kinetic_energy = arrays['initialKineticEnergy'][interacting_flag]
            interacting_target_length = arrays['targetLength'][interacting_flag]
            noninteracting_kinetic_energy = arrays['initialKineticEnergy'][~interacting_flag]
            noninteracting_target_length = arrays['targetLength'][~interacting_flag]

            interacting2d += np.histogram2d(interacting_kinetic_energy, interacting_target_length, bins=(x_bin_edges, y_bin_edges))[0]
            interacting += np.histogram(interacting_kinetic_energy, bin_edges)[0]
            interacting_step_size += np.histogram(interacting_target_length, bins=step_size_bin_edges)[0]
            noninteracting2d += np.histogram2d(noninteracting_kinetic_energy, noninteracting_target_length, bins=(x_bin_edges, y_bin_edges))[0]
            noninteracting += np.histogram(noninteracting_kinetic_energy, bin_edges)[0]
            noninteracting_weighted += np.histogram(noninteracting_kinetic_energy, bin_edges, weights=noninteracting_target_length)[0]
            noninteracting_step_size += np.histogram(noninteracting_target_length, bins=step_size_bin_edges)[0]

            continue

        # loop over entries
        for idx in range(steps):

            # progress bar
            count += 1
            progress.bar((count) / number_entries_)

            step_size = step_size_[idx]
            kinetic_energy = kinetic_energy_[idx]
            elastic = elastic_[idx]
            inelastic = inelastic_[idx]

            number_steps = len(step_size)

            if number_steps < 1:
                continue

            # print("kinetic_energy:", kinetic_energy)
            # print("step_size:", step_size)
            # print(elastic, inelastic)

            if adjust_steps:

                step_sizes = np.random.randint(1, max_step+1, size=number_steps)
                indices = np.cumsum(step_sizes)
                flag = indices < number_steps
                indices = indices[flag]

                indices = (number_steps-indices)[::-1]

                step_size = awkward.fromiter(np.split(step_size, indices)).sum()
                kinetic_energy = awkward.fromiter(np.split(kinetic_energy, indices))[:, 0]

            if elastic or inelastic:
                # empty2d = np.histogram2d([], [], bins=(x_bin_edges, y_bin_edges))[0]
                # print(kinetic_energy[-1:], step_size[-1:])
                # print(kinetic_energy[:-1], step_size[:-1])
                interacting2d += np.histogram2d(kinetic_energy[-1:], step_size[-1:], bins=(x_bin_edges, y_bin_edges))[0]
                noninteracting2d += np.histogram2d(kinetic_energy[:-1], step_size[:-1], bins=(x_bin_edges, y_bin_edges))[0]
                interacting += np.histogram(kinetic_energy[-1:], bin_edges)[0]
                noninteracting += np.histogram(kinetic_energy[:-1], bin_edges)[0]
                noninteracting_weighted += np.histogram(kinetic_energy[:-1], bin_edges, weights=step_size[:-1])[0]
                interacting_step_size += np.histogram(step_size[-1:], bins=step_size_bin_edges)[0]
                noninteracting_step_size += np.histogram(step_size[:-1], bins=step_size_bin_edges)[0]
            else:
                noninteracting2d += np.histogram2d(kinetic_energy, step_size, bins=(x_bin_edges, y_bin_edges))[0]
                noninteracting += np.histogram(kinetic_energy, bin_edges)[0]
                noninteracting_weighted += np.histogram(kinetic_energy, bin_edges, weights=step_size)[0]
                noninteracting_step_size += np.histogram(step_size, bins=step_size_bin_edges)[0]

            # print("interacting:", interacting, interacting.sum())
            # print("noninteracting:", noninteracting, noninteracting.sum())
            # print("interacting2d:", interacting2d, interacting2d.sum())
            # print("noninteracting2d:", noninteracting2d, noninteracting2d.sum())

            # input("\nPress Enter to continue...\n")

    output = {
        'interacting'      : interacting,
        'noninteracting'   : noninteracting,
        'noninteracting_weighted'   : noninteracting_weighted,
        'interacting2d'    : interacting2d,
        'noninteracting2d' : noninteracting2d,
        'interacting_step_size'    : interacting_step_size,
        'noninteracting_step_size' : noninteracting_step_size,
    }

    return output

if __name__ == '__main__':

    # output_dir = './output/'

    f = uproot.open(filename)

    hadron_tree = f['HadronAnalysis']
    g4xs_tree = f['CrossSection']

    g4ke = g4xs_tree.array('energy')
    g4xs = g4xs_tree.array('total')

    #mc = run(hadron_tree, entry_stop=1000)
    mc = run(hadron_tree)

    interacting = mc['interacting']
    noninteracting = mc['noninteracting']
    noninteracting_weighted = mc['noninteracting_weighted']

    interacting2d = mc['interacting2d']
    noninteracting2d = mc['noninteracting2d']

    # interacting_ = np.add.reduceat(interacting2d.sum(axis=1), np.arange(0, x_bins-1, 5)).astype(int)

    # interacting_step_size = interacting2d.sum(axis=0).astype(int)
    # noninteracting_step_size = noninteracting2d.sum(axis=0).astype(int)

    interacting_step_size = mc['interacting_step_size']
    noninteracting_step_size = mc['noninteracting_step_size']

    np.savetxt(output_dir+'interacting.txt', interacting, fmt='%d')
    np.savetxt(output_dir+'noninteracting.txt', noninteracting, fmt='%.18e')
    np.savetxt(output_dir+'noninteracting_weighted.txt', noninteracting_weighted, fmt='%.18e')
    np.savetxt(output_dir+'interacting2d.txt', interacting2d, fmt='%d')
    np.savetxt(output_dir+'noninteracting2d.txt', noninteracting2d, fmt='%d')
    np.savetxt(output_dir+'interacting_step_size.txt', interacting_step_size, fmt='%d')
    np.savetxt(output_dir+'noninteracting_step_size.txt', noninteracting_step_size, fmt='%d')

    # print("interacting:", interacting)
    # print("noninteracting:", noninteracting)

    # print("interacting_step_size:", interacting_step_size)
    # print("noninteracting_step_size:", noninteracting_step_size)

    # print("noninteracting2d:", noninteracting2d.sum(axis=0))

    # print(interacting2d.sum(), noninteracting2d.sum())

    mean_track_pitch = 0
    print("mean_track_pitch:", mean_track_pitch)
    mean_track_pitch += np.sum(step_size_bin_edges[:-1] * interacting_step_size)
    print("mean_track_pitch:", mean_track_pitch)
    mean_track_pitch += np.sum(step_size_bin_edges[:-1] * noninteracting_step_size)
    print("mean_track_pitch:", mean_track_pitch)
    mean_track_pitch /= np.sum(interacting_step_size + noninteracting_step_size)
    print("mean_track_pitch:", mean_track_pitch)

    input_list = [
        {
            'interacting': interacting,
            'noninteracting': noninteracting,
            #'mean_track_pitch': 0.4,
            'mean_track_pitch': mean_track_pitch,
            'n': n,
            #'label': 'Monte Carlo (simple ratio)',
            #'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (simple ratio)',
            'label': 'simple ratio',
            'color': 'C0',
        },
    ]

    plot.mc_model_ratio(input_list, bin_centers, g4ke=g4ke, g4xs=g4xs, savefig=output_dir+'mc_ratio.pdf')

    input_list = [
        {
            'interacting': interacting,
            'noninteracting': noninteracting_weighted,
            'mean_track_pitch': 1.0,
            'n': n,
            #'label': 'Monte Carlo (weighted ratio)',
            #'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (weighted ratio)',
            'label': 'weighted ratio',
            'color': 'C0',
        },
    ]

    plot.mc_model_ratio_w(input_list, bin_centers, g4ke=g4ke, g4xs=g4xs, savefig=output_dir+'mc_ratio_w.pdf')

    input_list = [
        {
            'interacting2d': (interacting2d, x_bin_edges, y_bin_edges),
            'noninteracting2d': (noninteracting2d, x_bin_edges, y_bin_edges),
            'n': n,
            #'label': 'Monte Carlo (likelihood)',
            #'label': '\\textsc{Geant}4 \\texttt{FTFP\char`_BERT} (likelihood)',
            'label': 'maximum likelihood estimation',
            'color': 'C0',
        },
    ]

    plot.mc_model_mle(input_list, bin_edges, g4ke=g4ke, g4xs=g4xs, savefig=output_dir+'mc_likelihood.pdf')

    plot.histogram1d(interacting_step_size, step_size_bin_edges,
                     'interacting step size [cm]',
                     'entries per {} cm'.format(step_size_bin_width),
                     savefig=output_dir+'interacting_step_size.pdf')

    plot.histogram1d(noninteracting_step_size, step_size_bin_edges,
                     'non-interacting step size [cm]',
                     'entries per {} cm'.format(step_size_bin_width),
                     savefig=output_dir+'noninteracting_step_size.pdf')

    plot.histogram1d(interacting_step_size+noninteracting_step_size,
                     step_size_bin_edges,
                     'step size [cm]',
                     'entries per {} cm'.format(step_size_bin_width),
                     savefig=output_dir+'step_size.pdf')

