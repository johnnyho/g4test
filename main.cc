#include "HadronDetectorConstruction.hh"
#include "HadronPrimaryGeneratorAction.hh"
#include "HadronRunAction.hh"
#include "HadronEventAction.hh"
#include "HadronSteppingAction.hh"

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "G4UIExecutive.hh"
// #include "G4VisExecutive.hh"

#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4UIterminal.hh"
#include "G4UItcsh.hh"

#include "G4PhysListFactory.hh"
#include "G4VModularPhysicsList.hh"
#include "G4StepLimiterPhysics.hh"

#include "G4Timer.hh"
#include "Randomize.hh"
#include "ctime"
#include "cstdio"

int main(int argc, char** argv)
{

    TFile * file = new TFile("output/tree.root", "recreate");
    TTree * tree = new TTree("tree", "i am a tree");

    float x;
    tree->Branch("x", &x, "x/F");
    tree->SetDirectory(file);

    for (unsigned int i = 0; i < 10000; ++i)
    {
      x = static_cast<float>(i);
      tree->Fill();
    }

    tree->Write();
    file->Close();

    G4Timer timer;
    timer.Start();
    time_t lt = time(NULL);
    std::cout<<"main: beginning time is"<<ctime(&lt)<<std::endl;

    // User Verbose output class
    //
    //G4VSteppingVerbose* verbosity = new NXSteppingVerbose;
    //G4VSteppingVerbose::SetInstance(verbosity);

    // choose the random engine
    G4Random::HepRandom::setTheEngine(new CLHEP::RanecuEngine());

    // Run manager
    //
    std::cout<<"main: init G4RunManager"<<std::endl;
    G4RunManager * runManager = new G4RunManager;

    // G4PhysListFactory factory;
    // G4VModularPhysicsList* physics = NULL;
    // G4String physicsListLabel = "FTFP_BERT";
    //-----------------------------------------------------
    // The following 19 physics lists are available:
    //  CHIPS
    //  FTFP_BERT
    //  FTFP_BERT_BERT_HP
    //  FTF_BIC
    //  LBE
    //  LHEP
    //  QBBC
    //  QGSC_BERT
    //  QGSP
    //  QGSP_BERT
    //  QGSP_BERT_CHIPS
    //  QGSP_BERT_HP
    //  QGSP_BIC
    //  QGSP_BIC_HP
    //  QGSP_FTFP_BERT
    //  QGS_BIC
    //  QGSP_INCLXX
    //  Shielding
    //-----------------------------------------------------
    // reference PhysicsList via its name
    // if (factory.IsReferencePhysList(physicsListLabel)) {
    //   physics = factory.GetReferencePhysList(physicsListLabel);
    // }

    // User Initialization classes (mandatory)
    //
    std::cout<<"main: detector init "<<std::endl;
    HadronDetectorConstruction* detector = new HadronDetectorConstruction;
    std::cout<<"main: detector user init"<<std::endl;
    runManager->SetUserInitialization(detector);
    //
    std::cout<<"main: physics loading"<<std::endl;
    //G4VUserPhysicsList* physics = new NXPhysicsList;
    G4PhysListFactory factory;
    G4VModularPhysicsList* physics = NULL;
    G4String physicsListLabel = "FTFP_BERT";
    if (factory.IsReferencePhysList(physicsListLabel)) {
      physics = factory.GetReferencePhysList(physicsListLabel);
      physics->RegisterPhysics(new G4StepLimiterPhysics());
    }
    std::cout<<"main: physics user init"<<std::endl;
    runManager->SetUserInitialization(physics);

    // User Action classes
    //
    std::cout<<"main: generator init "<<std::endl;
    G4VUserPrimaryGeneratorAction* generatorAction = new HadronPrimaryGeneratorAction(detector);
    std::cout<<"main: generator set action "<<std::endl;
    runManager->SetUserAction(generatorAction);
    //
    std::cout<<"main: run action init"<<std::endl;
    G4UserRunAction* runAction = new HadronRunAction;
    std::cout<<"main: run action user init"<<std::endl;
    runManager->SetUserAction(runAction);
    //
    G4UserEventAction* eventAction = new HadronEventAction;
    runManager->SetUserAction(eventAction);
    //
    G4UserSteppingAction* steppingAction = new HadronSteppingAction;
    runManager->SetUserAction(steppingAction);

    // Initialize G4 kernel
    //
    std::cout<<std::endl;
    std::cout<<"main: runManager init"<<std::endl;
    runManager->Initialize();

    // Get the pointer to the User Interface manager
    //
    G4UImanager * ui = G4UImanager::GetUIpointer();
    ui->ApplyCommand("/run/beamOn");
    //ui->ApplyCommand("/random/setSeeds 0 31");

    if (argc != 1) {
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        std::cout<<std::endl;
        std::cout<<"main: executing ..."<<std::endl;
        ui->ApplyCommand(command+fileName);
        std::cout<<"main: executing done"<<std::endl;
    } else {
        G4UIsession * session = 0;
        session = new G4UIterminal();
        session->SessionStart();
        delete session;
    }

    delete runManager;
    //delete verbosity;

    lt = time(NULL);
    printf( "the end time is %s", ctime(&lt));
    timer.Stop();
    std::cout<<"Spend time:"<<timer<<std::endl;

    return 0;
}

