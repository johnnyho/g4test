#ifndef HadronSensitiveDetector_h
#define HadronSensitiveDetector_h 1

#include "G4VSensitiveDetector.hh"

class G4Step;
class G4HCofThisEvent;

class HadronSensitiveDetector : public G4VSensitiveDetector
{
    public:

        HadronSensitiveDetector(G4String);
        ~HadronSensitiveDetector();

        void Initialize(G4HCofThisEvent*);
        G4bool ProcessHits(G4Step*, G4TouchableHistory*);
        void EndOfEvent(G4HCofThisEvent*);

    private:

        std::vector< std::string > elasticProcessNames;
        std::vector< std::string > inelasticProcessNames;
        std::vector< std::string > allowedProcessNames;
        std::vector< std::string > unallowedProcessNames;
        std::vector< std::string > processNames;

};
#endif

