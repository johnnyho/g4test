
#ifndef HadronDetectorMessenger_h
#define HadronDetectorMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class HadronDetectorConstruction;
class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithoutParameter;

class HadronDetectorMessenger : public G4UImessenger
{
    public:

        HadronDetectorMessenger(HadronDetectorConstruction*);
        virtual ~HadronDetectorMessenger();

        void SetNewValue(G4UIcommand*, G4String);

    private:

        HadronDetectorConstruction * detector;

        G4UIdirectory             * hadronDirectory;
        G4UIcmdWithAString        * worldMaterialCommand;
        G4UIcmdWithAString        * targetMaterialCommand;
        G4UIcmdWithADoubleAndUnit * targetRadiusCommand;
        G4UIcmdWithADoubleAndUnit * targetLengthCommand;
        G4UIcmdWithADoubleAndUnit * targetMaxStepCommand;
        G4UIcmdWithoutParameter   * updateCommand;

        // G4UIcmdWithAString        * matCmd;
        // G4UIcmdWithAString        * mat1Cmd;
        // G4UIcmdWithADoubleAndUnit * rCmd;
        // G4UIcmdWithADoubleAndUnit * lCmd;
        // G4UIcmdWithADoubleAndUnit * edepCmd;
        // G4UIcmdWithAnInteger      * binCmd;
        // G4UIcmdWithAnInteger      * nOfAbsCmd;
        // G4UIcmdWithAnInteger      * verbCmd;
        // G4UIcmdWithABool          * beamCmd;
        // G4UIcmdWithoutParameter   * updateCmd;
};

#endif
