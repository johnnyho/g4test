#ifndef HadronAnalysisManager_h
#define HadronAnalysisManager_h 1

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstdlib>
#include <cmath>

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"

#include "G4Material.hh"
#include "G4ParticleDefinition.hh"
#include "globals.hh"

class HadronAnalysisMessenger;

class HadronAnalysisManager {

    public:

        HadronAnalysisManager();
        ~HadronAnalysisManager();

        void Book(const G4int runID);
        void Save();
        void EventReset();
        void EventFill();

        void SetOutputDirectory(std::string value);
        void SetSaveSteps(bool value);

        void SetRun(const int value);
        void SetEvent(const int value);
        void SetInitialKineticEnergy(const double value);
        void SetElastic(const int value);
        void SetInelastic(const int value);
        void SetTargetRadius(const double value);
        void SetTargetLength(const double value);
        void SetTargetMaxStep(const double value);
        void AddKineticEnergy(const double value);
        void AddEnergyDeposit(const double value);
        void AddStepSize(const double value);

        G4String GetOutputDirectory();
        G4bool GetSaveSteps();

        bool GetElastic();
        bool GetInelastic();

        void SetTargetMaterial(G4Material* material);
        void SetParticle(G4ParticleDefinition* particleDefinition);

        G4String GetTargetMaterialName();
        G4String GetParticleName();

        void FillCrossSection(const double energy, const double eXS, const double iXS, const double tXS);

        static HadronAnalysisManager* getInstance();

    private:

        static HadronAnalysisManager* instance;

        G4String fileName;

        TFile* tfile;
        TTree* ttree;
        TTree* ttreeXS;

        int run;
        int event;
        double initialKineticEnergy;
        double targetRadius;
        double targetLength;
        double targetMaxStep;
        int elastic;
        int inelastic;
        std::vector< double > kineticEnergy;
        std::vector< double > energyDeposit;
        std::vector< double > stepSize;

        double energyXS;
        double elasticXS;
        double inelasticXS;
        double totalXS;

        G4Material* targetMaterial;
        G4String targetMaterialName;
        G4ParticleDefinition* particle;
        G4String particleName;

        std::string outputDirectory;
        bool saveSteps;

        HadronAnalysisMessenger * analysisMessenger;
};

#endif
