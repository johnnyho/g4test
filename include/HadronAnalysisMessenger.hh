#ifndef HadronAnalysisMessenger_h
#define HadronAnalysisMessenger_h 1

#include "globals.hh"
#include "G4UImessenger.hh"

class HadronAnalysisManager;
class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithABool;

class HadronAnalysisMessenger : public G4UImessenger
{
    public:

        HadronAnalysisMessenger(HadronAnalysisManager*);
        virtual ~HadronAnalysisMessenger();

        void SetNewValue(G4UIcommand*, G4String);

    private:

        HadronAnalysisManager * analysisManager;

        G4UIdirectory      * analysisDirectory;
        G4UIcmdWithAString * outputDirectoryCommand;
        G4UIcmdWithABool   * saveStepsCommand;
};

#endif
