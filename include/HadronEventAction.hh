
#ifndef HadronEventAction_h
#define HadronEventAction_h 1

#include "G4UserEventAction.hh"

class G4Event;

class HadronEventAction : public G4UserEventAction
{
    public:
        HadronEventAction();
        ~HadronEventAction();

    public:
        void BeginOfEventAction(const G4Event*);
        void EndOfEventAction(const G4Event*);
};

#endif

