#ifndef HadronPrimaryGeneratorAction_h
#define HadronPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"

class HadronDetectorConstruction;
class G4ParticleGun;
class G4Event;

class HadronPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
    public:
        HadronPrimaryGeneratorAction(HadronDetectorConstruction*);    
        ~HadronPrimaryGeneratorAction();

    public:
        void GeneratePrimaries(G4Event*);

    private:
        G4ParticleGun* particleGun;
        HadronDetectorConstruction* hadronDetector;
};

#endif
