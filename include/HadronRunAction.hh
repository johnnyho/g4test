#ifndef HadronRunAction_h
#define HadronRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"

class G4Run;

class HadronRunAction : public G4UserRunAction
{
    public:
        HadronRunAction();
        ~HadronRunAction();

    public:
        void BeginOfRunAction(const G4Run*);
        void EndOfRunAction(const G4Run*);
};

#endif





