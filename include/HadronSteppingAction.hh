
#ifndef HadronSteppingAction_h
#define HadronSteppingAction_h 1

#include "G4UserSteppingAction.hh"

class HadronSteppingAction : public G4UserSteppingAction
{
    public:
        HadronSteppingAction();
        ~HadronSteppingAction(){};

        void UserSteppingAction(const G4Step*);
};

#endif
