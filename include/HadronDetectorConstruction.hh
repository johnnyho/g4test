#ifndef HadronDetectorConstruction_h
#define HadronDetectorConstruction_h 1

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"

class G4Tubs;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;
class G4VPVParameterisation;
class G4UserLimits;

class HadronDetectorMessenger;

class HadronDetectorConstruction : public G4VUserDetectorConstruction
{
    public:

        HadronDetectorConstruction();
        ~HadronDetectorConstruction();

    public:

        G4VPhysicalVolume* Construct();

        void SetWorldMaterial(const G4String&);
        void SetTargetMaterial(const G4String&);

        void SetTargetRadius(G4double value);
        void SetTargetLength(G4double value);

        void SetTargetMaxStep(G4double value);

        void UpdateGeometry();

    private:

        G4Material*  worldMaterial;
        G4Material*  targetMaterial;

        G4Tubs*            solidWorld;     // pointer to the solid envelope 
        G4LogicalVolume*   logicWorld;     // pointer to the logical envelope
        G4VPhysicalVolume* physicalWorld;  // pointer to the physical envelope

        G4Tubs*            solidTarget;    // pointer to the solid Target
        G4LogicalVolume*   logicTarget;    // pointer to the logical Target
        G4VPhysicalVolume* physicalTarget; // pointer to the physical Target

        G4UserLimits* stepLimit;           // pointer to user step limits

        G4double maxStep;                  // max step size
        G4double targetRadius;             // radius of target
        G4double targetLength;             // length of target

        HadronDetectorMessenger * detectorMessenger;
};

#endif
