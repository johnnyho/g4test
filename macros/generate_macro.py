import numpy as np

filename = "run.mac"
n = 100  # number of events per run
# number_events_per_run = 100
particle = "pi-"
material = "G4_lAr"
target_radius = 50  # cm
target_length = 100  # cm
target_max_step = 0.05  # cm

start_energy = 1  # MeV
stop_energy = 2200  # MeV
delta_energy = 1  # MeV

start_target_length = 100  # cm
stop_target_length = 100  # cm
delta_target_length = 0.01  # cm

number_energies = (stop_energy-start_energy)/delta_energy + 1
energies = np.linspace(start_energy, stop_energy, number_energies)

number_target_lengths = (stop_target_length-start_target_length)/delta_target_length + 1
target_lengths = np.linspace(start_target_length, stop_target_length, number_target_lengths)

number_runs = number_energies * number_target_lengths

print "number of events per run:", n
print "number of runs:", number_runs
print "number of events:", n*number_runs

f = open(filename, 'w')

print >> f, "/control/verbose 1"
print >> f, "/run/verbose 1"
print >> f, "/tracking/verbose 0"
print >> f, ""
print >> f, "/runAnalysis/outputDirectory ./output"
print >> f, "/runAnalysis/saveSteps true"
print >> f, ""
print >> f, "/run/initialize"
print >> f, "/random/setSeeds 1 32"
print >> f, ""
print >> f, "/runHadron/worldMaterial G4_Galactic"
print >> f, "/runHadron/targetMaterial %s" % material
print >> f, "/runHadron/targetRadius %s cm" % target_radius
print >> f, "/runHadron/targetLength %s cm" % target_length
print >> f, "/runHadron/targetMaxStep %s cm" % target_max_step
print >> f, "/runHadron/update"
print >> f, ""
print >> f, "/gun/particle %s" % particle
print >> f, "/gun/position 0 0 -1"
print >> f, "/gun/direction 0 0 1"

for energy in energies:
    for target_length in target_lengths:
        print >> f, ""
        print >> f, "/runHadron/targetLength %s cm" % target_length
        print >> f, "/runHadron/update"
        print >> f, "/gun/energy %s MeV" % energy
        print >> f, "/run/beamOn %s" % n

f.close()

